package com.example.crowdsensing.sensorsubmission;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*
    * Sends http post request to web server to request for device key
    * This https request will be run on separate thread from main thread
    * using AsyncTask.
    *
    * input param[0]: deviceKey : String
    *
    * Result:
    * DeviceKey: String if post request success
    * null if post request failed
    *
    * */
public class WebServiceGetKeyAsyncTask extends AsyncTask<String, Integer, String> {

    private final String TEST_URL = "http://115.146.85.145/v1/get-device-key";

    private String requestDeviceKey() throws ClientProtocolException, IOException {
        Log.d("DataSubmission", "WebServiceGetKeyAsyncTask start");
        InputStream inputStream = null;
        String postResult = null;

        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(TEST_URL);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("secret", "12345678"));

        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        HttpResponse httpResponse = httpclient.execute(httpPost);
        if(httpResponse.getStatusLine().getStatusCode() == 200){
            Log.d("DataSubmission","Return code 200");

            // 3. receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // 4. convert inputstream to string
            if(inputStream != null){
                // assign secret key
                postResult = convertInputStreamToString(inputStream);
                Log.d("DataSubmission","Secret key "+postResult);
            }
            else
                postResult = null;

        }else{
            Log.d("DataSubmission","get seccret key failed");
        }

        Log.d("DataSubmission", "WebServiceGetKeyAsyncTask ends "+postResult);
        return postResult;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            String key = requestDeviceKey();
            Log.d("Request Key", key);
            return key;
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /*
    * Converts https response into text
    *
    * */
    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();

    }

}