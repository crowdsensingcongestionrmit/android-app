package com.example.crowdsensing.sensorsubmission;

import com.example.crowdsensing.MySQLite;
import com.example.crowdsensing.json.JSONBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * This class acts as a util class for DataSubmissionIntentService to send data to web server.
 * The actual code for sending data is in this class.
 *
 * Uses aynctask to send data through http post. Each time it will retrieve 50 rows from each sensor
 * data to be submitted to web server.
 */
public class DataSubmitter {

    private final int retriveRowNo = 50;
    private JSONBuilder jsonBuilder;
    private MySQLite db;

    /**
     * Constructs an object is used in DataSubmissionIntentService to submit date to web server.
     * @param db the Database object which needs to be use to retrieve and sendata
     */
    public DataSubmitter(MySQLite db){
        jsonBuilder = new JSONBuilder();
        this.db = db;
    }

    /**
     * This method is used by different method for sending sensor data to webserver
     *
     * @param deviceKey unique key for different device which should be saved in shared preference
     * @param jsonObject the sensor data which is transformed into JSON format
     * @return The true if the data is sent to the web server successfully
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    public boolean sendData(String deviceKey, JSONObject jsonObject) throws InterruptedException, ExecutionException, JSONException {
        return new WebServiceStoreAsyncTask().execute(deviceKey,jsonObject).get();
    }

    /**
     * Uses different method (e.g. sendGPS()) for posting different sensor data to web server.
     * This is used by DataSubmissionIntentService to post all sensor data to web server.
     *
     * @param deviceKey unique key for different device which should be saved in shared preference
     */
    public void postData(String deviceKey){
        try {
            sendGPS(deviceKey);
            sendWifi(deviceKey);
            sendAccelerometer(deviceKey);
            sendMagnetometer(deviceKey);
            sendBarometer(deviceKey);
            sendTemperature(deviceKey);
            sendLabel(deviceKey);
            sendBluetoothDevice(deviceKey);
            sendCellTower(deviceKey);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is to be used in postData method to retrieve GPS data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendGPS(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject gpsReadingJSON = db.getGPSReadingJSON(retriveRowNo);
        JSONArray gpsReadingJSONJSONArray = gpsReadingJSON.getJSONArray("GPSReadings");

        if(gpsReadingJSONJSONArray.length() == 0)
            return;

        boolean gpsSuccess = false;

        long gpsMaxTime = gpsReadingJSONJSONArray.getJSONObject(gpsReadingJSONJSONArray.length()-1).getLong("ScanTime");

        gpsSuccess = sendData(deviceKey,gpsReadingJSON);
        if(gpsSuccess){
            db.deleteGPSReadingsUntil(gpsMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Wifi data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendWifi(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject wifiScansJSON = db.getWiFiScansJSON(retriveRowNo);
        JSONArray  wifiScansJSONArray = wifiScansJSON.getJSONArray("WifiScans");

        if(wifiScansJSONArray.length() == 0)
            return;

        boolean wifiSuccess = false;
        long wifiMaxTime = wifiScansJSONArray.getJSONObject(wifiScansJSONArray.length()-1).getJSONObject("WifiScan").getLong("ScanEndTime");

        wifiSuccess = sendData(deviceKey,wifiScansJSON);
        if(wifiSuccess){
            db.deleteWiFiScansUntil(wifiMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Accelerometer data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendAccelerometer(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject accelerometerReadingJSON = db.getAccelerometerReadingJSON(retriveRowNo);
        JSONArray  accelerometerReadingJSONArray = accelerometerReadingJSON.getJSONArray("Accelerometer");

        if(accelerometerReadingJSONArray.length() == 0)
            return;

        boolean accelerometerSuccess = false;
        long accelerometerMaxTime = accelerometerReadingJSONArray.getJSONObject(accelerometerReadingJSONArray.length()-1).getLong("timestamp");

        accelerometerSuccess = sendData(deviceKey,accelerometerReadingJSON);
        if(accelerometerSuccess){
            db.deleteAccelerometerReadingsUntil(accelerometerMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Magnetometer data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendMagnetometer(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject magnetometerReadingJSON = db.getMagnetometerReadingJSON(retriveRowNo);
        JSONArray magnetometerReadingJSONArray = magnetometerReadingJSON.getJSONArray("Magnetometer");

        if(magnetometerReadingJSONArray.length() == 0)
            return;

        boolean magnetometerSuccess = false;

        long magnetometerMaxTime = magnetometerReadingJSONArray.getJSONObject(magnetometerReadingJSONArray.length()-1).getLong("timestamp");

        magnetometerSuccess = sendData(deviceKey,magnetometerReadingJSON);
        if(magnetometerSuccess){
            db.deleteMagnetometerReadingsUntil(magnetometerMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Barometer data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendBarometer(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject barometerReadingJSON = db.getBarometerReadingJSON(retriveRowNo);
        JSONArray barometerReadingJSONArray = barometerReadingJSON.getJSONArray("Barometer");

        if(barometerReadingJSONArray.length() == 0)
            return;

        boolean barometerSuccess = false;

        long barometerMaxTime = barometerReadingJSONArray.getJSONObject(barometerReadingJSONArray.length()-1).getLong("timestamp");

        barometerSuccess = sendData(deviceKey,barometerReadingJSON);
        if(barometerSuccess){
            db.deleteBarometerReadingsUntil(barometerMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Temperature data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendTemperature(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject temperatureReadingJSON = db.getTemperatureReadingJSON(retriveRowNo);
        JSONArray temperatureReadingJSONArray = temperatureReadingJSON.getJSONArray("Temperature");

        if(temperatureReadingJSONArray.length() == 0)
            return;

        boolean temperatureSuccess = false;

        long temperatureMaxTime = temperatureReadingJSONArray.getJSONObject(temperatureReadingJSONArray.length()-1).getLong("timestamp");

        temperatureSuccess = sendData(deviceKey,temperatureReadingJSON);
        if(temperatureSuccess){
            db.deleteTemperatureReadingsUntil(temperatureMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Label data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendLabel(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject labelJSON = db.getTextEventsJSON(retriveRowNo);
        boolean labelSuccess = false;

        JSONArray array = labelJSON.getJSONArray("Event");

        if(array.length() == 0)
            return;

        JSONObject lastElement = array.getJSONObject(array.length()-1);
        long endTime = lastElement.getLong("timestamp");

        labelSuccess = sendData(deviceKey,labelJSON);
        if(labelSuccess){
            db.deleteTextEventsUntil(endTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve Bluetooth data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendBluetoothDevice(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject bluetoothJSON = db.getBluetoothJSON(retriveRowNo);

        boolean bluetoothSuccess = false;

        JSONArray bluetoothJSONJSONArray = bluetoothJSON.getJSONArray("Bluetooth");

        if(bluetoothJSONJSONArray.length() == 0)
            return;
        long bluetoothMaxTime = bluetoothJSONJSONArray.getJSONObject(bluetoothJSONJSONArray.length()-1).getLong("timestamp");

        bluetoothSuccess = sendData(deviceKey,bluetoothJSON);
        if(bluetoothSuccess){
            db.deleteBluetoothUntil(bluetoothMaxTime);
        }

    }

    /**
     * This method is to be used in postData method to retrieve CellTower data as JSON object and
     * delete from database if the data is submitted to web server successfully. Uses sendData
     * method to trigger asynctask to send data to web server.
     *
     * @param deviceKey unique key for different device to uniquely identify the the device
     * @throws InterruptedException When the asynctask is interrupted
     * @throws ExecutionException  When executing asynctask
     * @throws JSONException When JSON could not be built
     */
    private void sendCellTower(String deviceKey) throws JSONException, InterruptedException, ExecutionException {
        JSONObject cellReadingJSON = db.getCellReadingJSON(retriveRowNo);

        boolean cellReadingSuccess = false;

        JSONArray cellReadingJSONArray = cellReadingJSON.getJSONArray("CellTowerReadings");

        if(cellReadingJSONArray.length() == 0)
            return;
        long cellReadingMaxTime = cellReadingJSONArray.getJSONObject(cellReadingJSONArray.length()-1).getLong("ScanTime");

        cellReadingSuccess = sendData(deviceKey,cellReadingJSON);
        if(cellReadingSuccess){
            db.deleteCellReadingsUntil(cellReadingMaxTime);
        }

    }
}
