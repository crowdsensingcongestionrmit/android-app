package com.example.crowdsensing.sensorsubmission;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

    /**
     * Sends http post request to send sensor data to web server
     * This https request will be run on separate thread from main thread
     * using AsyncTask.
     *
     * param[0]: device key:String
     * param[1]: JSON to be send to web server: JSONObject
     *
     * Return:
     * False if post request failed
     * True if post request success
     */
public class WebServiceStoreAsyncTask extends AsyncTask<Object, Integer, Boolean> {

    private final String STORE_URL = "http://115.146.85.145/v1/store";

    private Boolean store(String deviceKey,JSONObject postJSON) throws ClientProtocolException, IOException {

        InputStream inputStream = null;
        Boolean postResult = false;
//        Log.d("DataSubmission", "store start : " + postResult+" with key "+ deviceKey);
        // 1. create HttpClient
        HttpClient httpclient = new DefaultHttpClient();

        // 2. make POST request to the given URL
        HttpPost httpPost = new HttpPost(STORE_URL);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("key", deviceKey));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // 3. convert JSONObject to JSON to String
        String jsonString = postJSON.toString();
        nameValuePairs.add(new BasicNameValuePair("data", jsonString));
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // 4. Execute POST request to the given URL
        HttpResponse httpResponse = httpclient.execute(httpPost);

        if(httpResponse.getStatusLine().getStatusCode() == 200){
            postResult = true;
        }
//        Log.d("DataSubmission","store end : "+postResult);
        return postResult;
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        // TODO Auto-generated method stub
        String key = params[0].toString();
        JSONObject postJSON = (JSONObject) params[1];

        try {
            return store(key,postJSON);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}