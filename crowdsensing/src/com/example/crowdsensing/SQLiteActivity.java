package com.example.crowdsensing;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import java.util.List;

public class SQLiteActivity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite);
 
        MySQLite db = new MySQLite(this);
 
        /**
         * CRUD Operations
         * */
 
//        WifiReading reading = new WifiReading("a4:b1:e9:ed:b5:c8","testing",48,1403527598000l);
//        Set<WifiReading> set = new HashSet<WifiReading>();
//        set.add(reading);
//        db.addGPSReading(new GPSReading(-37.76750903021238, 144.9743137392988, 32.0f, 1403527598000l));
//        db.addWifiScan(new WifiScan(1403527598002l,1403527599995l,set));
//        // get all books
//        db.deleteGPSReadingsUntil(1403527598000l);
//        db.deleteWiFiScansUntil(1403527598002l);
        
        List<GPSReading> list = db.getGPSReading(100);
        List<WifiScan> list2 = db.getWiFiScans(100);
 
        // delete one book
//        db.deleteBook(list.get(0));
// 
//        // get all books
//        db.getAllBooks();
//        
//        list = db.getAllBooks();
        
        TextView sensorsData = (TextView)findViewById(R.id.textView1);
        
        StringBuilder data = new StringBuilder();
	      for(GPSReading position: list){
	         data.append("longitude: " + position.getLongitude() + "\n");
	         data.append("latitude: " + position.getLatitude() + "\n");
	         data.append("accuracy: " + position.getAccuracy() + "\n");
	         data.append("time: "+position.getTime() + "\n");
	      }
	      data.append("\n\n");
	      for(WifiScan scan:list2){
	    	data.append("startTime: "+scan.getStartTime() + "\n");
	    	data.append("endTime: "+scan.getEndTime() + "\n");
	    	data.append("With wifi Reading:\n");
	    	for(WifiReading wifireading: scan.getReadings())
	    	{
		    	data.append("macAddress: "+wifireading.getMacAddress()+ "\n");
		    	data.append("ssid: "+wifireading.getName() + "\n");
		    	data.append("signal level: "+wifireading.getSignalLevel() + "\n");
		    	data.append("time: "+wifireading.getTime() + "\n");
	    	}
	    	data.append("End..........\n");
	      }
	   sensorsData.setText(data);
 
    }
}
