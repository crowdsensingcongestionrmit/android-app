package com.example.crowdsensing;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.crowdsensing.broadcastreceiver.DataSubmissionAlarmReceiver;


public class MainActivity extends Activity {

    private String deviceKey;
    private MySQLite db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        db = new MySQLite(this);
        scheduleDataSubmission();
        setDeviceKeyTextView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		startService(new Intent(this, DataCollectionService.class));
		return true;
	}
	
	protected void onResume() {
		super.onResume();
	}
	
	protected void onPause() {
		super.onPause();
        db.close();
	}
	
	public void showSensor(View v)
	{
		Intent intent = new Intent(this, DisplaySensorActivity.class);
		startActivity(intent);
	}
	
	public void sqlite_demo(View v)
	{
		Intent intent = new Intent(this, SQLiteActivity.class);
		startActivity(intent);
	}

    /*
     * Schedules AlarmManager to submit data to web server every x intervals
     * 
     */
    private void scheduleDataSubmission(){

    	AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
    	Intent intent = new Intent(this,DataSubmissionAlarmReceiver.class);

        // checks if alarm has already been scheduled
        boolean alarmUp = (PendingIntent.getBroadcast(this.getApplicationContext(), 1234567, intent,PendingIntent.FLAG_NO_CREATE) != null);
        if(alarmUp){
            Log.d("scheduleDataSubmission", "Alarm for submission have been scheduled!!");
            return;
        }

        Log.d("scheduleDataSubmission", "schedule dataSubmission");
    	//create a pending intent to be called
    	PendingIntent  dataSubmissionPI= PendingIntent.getBroadcast(this.getApplicationContext(), 1234567, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    	//schedule time for pending intent, and set the interval to 30 seconds
    	alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 1*30*1000, dataSubmissionPI);

    }

    private void retrieveDeviceKey(){
        // get device key from sharedPreference
        SharedPreferences pref;
        pref = getSharedPreferences("com.example.crowdsensing", MODE_MULTI_PROCESS);
        deviceKey = pref.getString("com.example.crowdsensing.key", null);
    }

    private void setDeviceKeyTextView(){
        TextView deviceKeyTextView = (TextView)findViewById(R.id.device_key_view);
        retrieveDeviceKey();
        if(deviceKey != null){
            deviceKeyTextView.setText(deviceKey);
        }
    }

    public void labelSensor(View v){
        TextView currentLabelTextView = (TextView)findViewById(R.id.currenLabel);
        EditText labelEditText = (EditText)findViewById(R.id.labelEditText);
        Button labelSensorBtn = (Button)findViewById(R.id.labelButton);
        Button unlabelSensorBtn = (Button)findViewById(R.id.unlabelButton);

        labelSensorBtn.setEnabled(false);
        labelEditText.setEnabled(false);
        currentLabelTextView.setText(labelEditText.getText().toString());
        unlabelSensorBtn.setEnabled(true);
        db.addTextEvent(labelEditText.getText().toString());
    }

    public void unlabelSensor(View v){
        TextView currentLabelTextView = (TextView)findViewById(R.id.currenLabel);
        EditText labelEditText = (EditText)findViewById(R.id.labelEditText);
        Button labelSensorBtn = (Button)findViewById(R.id.labelButton);
        Button unlabelSensorBtn = (Button)findViewById(R.id.unlabelButton);

        labelSensorBtn.setEnabled(true);
        labelEditText.setEnabled(true);
        currentLabelTextView.setText(R.string.default_current_label_text);
        unlabelSensorBtn.setEnabled(false);
    }

    public void collectControl(View v){
        final Button collectBtn = (Button)findViewById(R.id.collectButton);
        collectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(collectBtn.getText().toString().compareTo(getApplicationContext().getString(R.string.collect_start_label))==0){
                    startService(new Intent(getApplicationContext(), DataCollectionService.class));
                    collectBtn.setText(R.string.collect_stop_label);
                }
                else if(collectBtn.getText().toString().compareTo(getApplicationContext().getString(R.string.collect_stop_label))==0){
                    stopService(new Intent(getApplicationContext(), DataCollectionService.class));
                    collectBtn.setText(R.string.collect_start_label);
                }
            }
        });
    }

    public void walking(View view){

        db.addTextEvent("walking");
    }

    public void standing(View view){
        db.addTextEvent("standing");
    }

    public void gettingOnTram(View view){
        db.addTextEvent("getting on tram");
    }

    public void gettingOffTram(View view){
        db.addTextEvent("getting off tram");
    }

    public void tramMoving(View view){
        db.addTextEvent("tram moving");
    }

    public void tramStopped(View view){
        db.addTextEvent("tram stopped");
    }

    public void gettingOnTrain(View view){
        db.addTextEvent("getting on train");
    }

    public void gettingOffTrain(View view){
        db.addTextEvent("getting off train");
    }

    public void trainMoving(View view){
        db.addTextEvent("train moving");
    }

    public void waitingTram(View view){
        db.addTextEvent("waiting tram");
    }

    public void trainStopped(View view){
        db.addTextEvent("train stopped");
    }

    public void waitingTrain(View view){
        db.addTextEvent("waiting train");
    }

    public void doorOpened(View view){
        db.addTextEvent("door opened");
    }

    public void doorClosed(View view){
        db.addTextEvent("door closed");
    }


}
