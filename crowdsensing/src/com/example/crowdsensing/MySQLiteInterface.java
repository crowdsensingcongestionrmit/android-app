package com.example.crowdsensing;

import android.database.sqlite.SQLiteDatabase;

import com.example.crowdsensing.sensorreading.AccelerometerReading;
import com.example.crowdsensing.sensorreading.BarometerReading;
import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.MagnetometerReading;
import com.example.crowdsensing.sensorreading.TemperatureReading;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public interface MySQLiteInterface {
	
	/*some of the method below looks similar, because there's two version of these method
	 *the old version and the optimized version of the method, the old version of the method
	 *will be removed soon*/
	public void onCreate(SQLiteDatabase db);
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);
	/*below is the method for storing data into local database storage*/
	public void addGPSReading(GPSReading position);
	public void addGPSReading(double longi,double lati,float accuracy,long time);
	public void addWifiScan(WifiScan scan);
	public void addWifiScan(long starttime,long endtime,List<WifiReading> readings);
	public void addAccelerometerReading(AccelerometerReading accelerometer);
	public void addAccelerometerReading(long time, float xvalue, float yvalue, float zvalue);
	public void addMagenetometerReading(MagnetometerReading magnetometer);
	public void addMagenetometerReading(long time, float xvalue, float yvalue, float zvalue);
	public void addBarometerReading(BarometerReading barometer);
	public void addBarometerReading(long time, float pressure);
	public void addTemperatureReading(TemperatureReading temperature);
	public void addTemperatureReading(long time, float degreeCelcius);
	/*below is the method for retrieving and deleting data stored in local database*/
	public List<GPSReading> getGPSReading(int limit);
	public JSONObject getGPSReadingJSON(int limit) throws JSONException;
	public void deleteGPSReadingsUntil(long time);
	public List<WifiScan> getWiFiScans(int limit);
	public JSONObject getWiFiScansJSON(int limit) throws JSONException;
	public void deleteWiFiScansUntil(long endTime);
	public List<AccelerometerReading> getAccelerometerReading(int limit);
	public JSONObject getAccelerometerReadingJSON(int limit) throws JSONException;
	public void deleteAccelerometerReadingsUntil(long time);
	public List<MagnetometerReading> getMagnetometerReading(int limit);
	public JSONObject getMagnetometerReadingJSON(int limit) throws JSONException;
	public void deleteMagnetometerReadingsUntil(long time);
	public List<BarometerReading> getBarometerReading(int limit);
	public JSONObject getBarometerReadingJSON(int limit) throws JSONException;
	public void deleteBarometerReadingsUntil(long time);
	public List<TemperatureReading> getTemperatureReading(int limit);
	public JSONObject getTemperatureReadingJSON(int limit) throws JSONException;
	public void deleteTemperatureReadingsUntil(long time);

}
