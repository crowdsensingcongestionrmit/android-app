package com.example.crowdsensing.sensorcollection.pullsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * This class is used for collecting Accelerometer sensor data.
 * It extends the Android's SensorManager to starts and stops Accelerometer
 * sensor collection.
 */
public class AccelerometerSensor extends PullSensor {
    private Sensor mAccelerometer;

    /**
     * Constructs an object that can be used to start and stop accelerometer
     * sensor collection.
     * <p>
     *
     * @param context  the context used to create Android Accelerometer sensor.  If
     *                 the context is null, the object returned cannot be used to start
     *                 collection.
     *
     * @param eListener the SensorDataEventListener used to store Accelerometer into
     *                  database. If the eListener is null, the sensor data collection
     *                  will not be stored in database.
     */
    public AccelerometerSensor(Context context, SensorEventListener eListener) {
        super(context,eListener);
    }

    /**
     * Starts the accelerometer sensor collection with SENSOR_DELAY_NORMAL
     * sensitivity. The sensor data collected will be stored in database.
     */
    public void register() {
        mAccelerometer = getSensorManager().getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        super.register(mAccelerometer,SensorManager.SENSOR_DELAY_NORMAL);
    }

    /**
     * Stops the accelerometer sensor collection
     */
    public void unregister() {
        super.unregister(mAccelerometer);
    }

}
