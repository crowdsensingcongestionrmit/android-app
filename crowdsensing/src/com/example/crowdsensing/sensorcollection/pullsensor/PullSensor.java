package com.example.crowdsensing.sensorcollection.pullsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.example.crowdsensing.sensorcollection.CollectSensor;

/**
* This class should be extended by Sensor class that uses SensorEventListener for sensor readings
* updates.
*
* This class provides generic method for registering and unregistering sensor for collecting
* sensor data.
*/
public abstract class PullSensor extends CollectSensor {

    private Context context;
    private SensorManager mSensorManager;
    private SensorEventListener eListener;

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create Android SensorManager object to start the sensor
     *                 collection. If the context is null, the object returned cannot be used to
     *                 start collection.
     *
     * @param eListener the SensorDataEventListener used to store Sensor data into database.
     *                  If the eListener is null, the sensor data collection will not be stored
     *                  in database.
     */
    public PullSensor(Context context, SensorEventListener eListener) {
        this.context = context;
        this.eListener = eListener;
        mSensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
    }

    /**
     * Returns the context used to create SensorManager
     * <p>
     *
     * @return      the Context used in object construction parameter
     */
    public Context getContext(){
        return context;
    }

    /**
     * Returns the SensorManager used to start and stop Sensor
     * <p>
     *
     * @return      the SensorManager created used to start and stop Sensor
     */
    public SensorManager getSensorManager(){
        return mSensorManager;
    }

    /**
     * Returns the SensorEventListener used listen to sensor data and store sensor data to database
     * <p>
     *
     * @return      the SensorEventListener used to start listen to sensor data and store sensor
     * data to database
     */
    public SensorEventListener getEventListener(){
        return eListener;
    }

    /**
     * Starts the sensor collection and the sensor data collected will be stored in database.
     *
     * @param sensor  the sensor object that needs to be started for the sensor collection.
     *                If the context is null, the object returned cannot be used to start
     *                collection.
     *
     * @param sensitivityLevel the sensitivityLevel level that the Sensor will be used to collect
     *                         sensor data. The sensitivity level is be predefined in SensorManager
     *                         as static variable.
     */
    public void register(Sensor sensor, int sensitivityLevel){
        if(sensor != null){
            mSensorManager.registerListener(eListener,sensor,sensitivityLevel);
        }
    }

    /**
     * Stops the sensor collection
     *
     * @param sensor the Sensor that will needs to be stopped from collecting sensor data.
     */
    public void unregister(Sensor sensor){
        mSensorManager.unregisterListener(eListener, sensor);
    }
}
