package com.example.crowdsensing.sensorcollection.pullsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


public class BarometerSensor extends PullSensor {

    private Sensor barometerSensor;

    public BarometerSensor(Context context, SensorEventListener eListener) {
        super(context,eListener);
    }

    public void register() {
        barometerSensor = getSensorManager().getDefaultSensor(Sensor.TYPE_PRESSURE);
        super.register(barometerSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregister() {
        super.unregister(barometerSensor);
    }

}
