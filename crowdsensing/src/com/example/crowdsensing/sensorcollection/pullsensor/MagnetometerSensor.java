package com.example.crowdsensing.sensorcollection.pullsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MagnetometerSensor extends PullSensor {

    private Sensor mMagnetometer;

    public MagnetometerSensor(Context context, SensorEventListener eListener) {
        super(context,eListener);
    }

    public void register() {
        mMagnetometer = getSensorManager().getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        super.register(mMagnetometer,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregister() {
        super.unregister(mMagnetometer);
    }
}
