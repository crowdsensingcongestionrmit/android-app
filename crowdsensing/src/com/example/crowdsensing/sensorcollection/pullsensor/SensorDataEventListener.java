package com.example.crowdsensing.sensorcollection.pullsensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import com.example.crowdsensing.MySQLite;

/**
 * This class is used by Sensor class that will broadcast
 * sensor reading update via SensorEvent. When adding new sensor
 * to the app, an instance of this class can be instantiated
 * and registered to SensorManager.registerListener.
 * 
 * Sensor Readings manipulation can be done in onSensorChanged methods.
 * 
 */
public class SensorDataEventListener implements SensorEventListener {

    private MySQLite db;

    /**
     * Constructs an SensorDataEventListener object that can be used for Sensor object
     * instantiation.
     * <p>
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    public SensorDataEventListener(MySQLite db) {
        this.db = db;
    }

    /**
     * <p>
     *     This method will called when the sensor data is received via the SensorEventListener
     *     Sensor data manipulation can be done within this method. Sensor data will be stored in
     *     the database when sensor data is received via SensorEvent.
     *     <p/>
     * @param event event which allows the app to retrieve sensor data. Refer to Android's Sensor
     *              Event to more details.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        // TODO Auto-generated method stub
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//            Log.d("PullSensor","TYPE_ACCELEROMETER");
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            db.addAccelerometerReading(System.currentTimeMillis(),x,y,z);
        }
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//            Log.d("PullSensor","TYPE_MAGNETIC_FIELD");
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            db.addMagenetometerReading(System.currentTimeMillis(),x,y,z);
        }
        if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
//            Log.d("PullSensor","TYPE_AMBIENT_TEMPERATURE");
            float temperature = event.values[0];

            db.addTemperatureReading(System.currentTimeMillis(),temperature);
        }
        if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
//            Log.d("PullSensor","TYPE_PRESSURE");
            float pressure = event.values[0];
            db.addBarometerReading(System.currentTimeMillis(),pressure);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }


}
