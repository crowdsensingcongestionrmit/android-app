package com.example.crowdsensing.sensorcollection.pullsensor;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class TemperatureSensor extends PullSensor {

    private Sensor mTemparature;

    public TemperatureSensor(Context context, SensorEventListener eListener) {
        super(context, eListener);

    }

    public void register() {
        mTemparature = getSensorManager().getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        super.register(mTemparature,SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregister() {
        super.unregister(mTemparature);
    }
}
