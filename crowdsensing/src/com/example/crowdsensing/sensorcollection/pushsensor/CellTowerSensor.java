package com.example.crowdsensing.sensorcollection.pushsensor;


import android.content.Context;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;

import com.example.crowdsensing.MySQLite;

/**
 * This class is used for collecting CellTowerSensor data sensor data.
 * It extends the PushSensor to starts and stops CellTower Sensor
 * collection. The CellTowerSensor will scans for cell tower every 5s.
 */
public class CellTowerSensor extends PushSensor{

    private GsmCellLocation gsmCellLocation;
    private TelephonyManager telephonyManager;
    private Handler handler = new Handler();
    private long scanCellTowerInterval = 5000;

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create the TelephonyManager for sensor collection.
     *                 If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    public CellTowerSensor(Context context, MySQLite db) {
        super(context, db);
        telephonyManager = (TelephonyManager)getContext().getSystemService(Context.TELEPHONY_SERVICE);
    }

    /**
    * Used to scan cell tower every X interval executed by Handler
    */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            scanCellTower();
            /* reschedule the runnable task to run in next time set in scanWifiInterval*/
            handler.postDelayed(this, scanCellTowerInterval);

        }
    };


    private void scanCellTower(){
        String networkOperator = telephonyManager.getNetworkOperator();
        int mcc;
        int mnc;
        int cid;
        int lac;
        if(networkOperator.length()==0){
            mcc = 0;
            mnc = 0;
            cid = 0;
            lac = 0;
        }else{
            mcc = Integer.parseInt(networkOperator.substring(0, 3));
            mnc = Integer.parseInt(networkOperator.substring(3));
            gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
            cid = gsmCellLocation.getCid();
            lac = gsmCellLocation.getLac();
        }
        // add to database
        getDb().addCellTowerReading(cid,lac,mcc,mnc,System.currentTimeMillis());
    }

    /**
     * Starts the Cell Tower data collection and data collected will be stored in database.
     * The sensor will store device Connected Cell Tower mcc, mnc, cid, lac.
     */
    @Override
    public void register() {
        handler.postDelayed(runnable, scanCellTowerInterval);
    }

    /**
     * Stops the  Cell Tower data collection.
     */
    @Override
    public void unregister() {
        handler.removeCallbacks(runnable);
    }
}
