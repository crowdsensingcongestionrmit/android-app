package com.example.crowdsensing.sensorcollection.pushsensor;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.example.crowdsensing.MySQLite;
import com.example.crowdsensing.sensorreading.GPSReading;

/**
 * This class is used for collecting LocationSensor data sensor data.
 * It extends the PushSensor to starts and stops LocationSensor Sensor
 * collection. The LocationSensor will update location for every 1 s and 0m travelled.
 *
 */
public class LocationSensor extends PushSensor {

	private LocationManager locationManager;
	//will update location every 1 s and 0m travelled
	private int MIN_UPDATE_TIME = 1000;
	private int MIN_UPDATE_DIST = 0;

	// Used as unique name space for broadcasting intent
	public static final String ACTION_LOCATION_BROADCAST = "com.example.crowdsensing.DataCollectionService",
			EXTRA_LATITUDE = "extra_latitude",
			EXTRA_LONGITUDE = "extra_longitude",
			EXTRA_PROVIDER = "extra_provider",
			EXTRA_ACCURACY = "extra_accuracy",
			EXTRA_TIME = "extra_time";

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create and start the sensor collection.
     *                 If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
	public LocationSensor(Context context,MySQLite db) {
        super(context,db);
        locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
	}

    /**
     * create LocationListener to be used by this service DataCollectionService
     * to update location. Adds location data to database when update is received.
     */
	private LocationListener locationListener = new LocationListener() {

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			//sendBroadcastMessage(location);
			getDb().addGPSReading(location.getLongitude(),location.getLatitude(),location.getAccuracy(),location.getTime());
		}
	};

    /**
     * Starts the Location data collection and data collected will be stored in database.
     * The sensor will store location longitude, latitude, accuracy, location data scan time.
     * The sensor will try to get location from NETWORK_PROVIDER, GPS_PROVIDER and PASSIVE_PROVIDER.
     */
    public void register(){
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DIST, locationListener);
    }

    /**
     * Stops the Location data collection.
     */
    public void unregister(){
        locationManager.removeUpdates(locationListener);
    }

	// Broadcast location detail and wifi APs to Receiver
	// Scans wifi Access Point before broadcasting message
	private void sendBroadcastMessage(Location location){
		if(location !=null){
			//populates intent
			Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
			intent.putExtra(EXTRA_LATITUDE, location.getLatitude());
			intent.putExtra(EXTRA_LONGITUDE, location.getLongitude());
			intent.putExtra(EXTRA_PROVIDER, location.getProvider());
			intent.putExtra(EXTRA_ACCURACY, location.getAccuracy());
			intent.putExtra(EXTRA_TIME, location.getTime());

			LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
		}
	}
}
