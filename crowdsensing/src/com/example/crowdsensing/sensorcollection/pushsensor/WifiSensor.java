package com.example.crowdsensing.sensorcollection.pushsensor;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;

import com.example.crowdsensing.MySQLite;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used for collecting WifiSensor sensor data.
 * It extends the PushSensor to starts and stops LocationSensor Sensor
 * collection. The WifiSensor will scan for wifi AP every 5s.
 */
public class WifiSensor extends PushSensor {

    private static WifiSensor _instance;
    private ArrayList<String> wifiAP;
    private WifiManager wifiManager;
    private ConnectivityManager connMgr;
    private NetworkInfo networkInfo;
    private List<WifiReading> readings;
    private Handler handler = new Handler();
    private long scanWifiInterval = 5000;


    // Used as unique name space for broadcasting intent
    private static final String ACTION_WIFISCAN_BROADCAST = "com.example.crowdsensing.WifiScanBroadcastReceiver",
            EXTRA_WIFI_AP = "extra_wifi_ap",
            EXTRA_WIFI_AP_TIME = "extra_wifi_ap_time";

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create and start the sensor collection.
     *                 If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    private WifiSensor(Context context,MySQLite db) {
        super(context,db);

        wifiAP = new ArrayList<String>();
        readings = new ArrayList<WifiReading>();
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static WifiSensor getInstance(Context context,MySQLite db){

        if(_instance == null){
            _instance = new WifiSensor(context,db);
        }
        return _instance;
    }

    /**
     * Receiver for wifimanager.startscanning()
     * Invoke by wifimanager.startscanning() when finished scanning
     * Reads Wifi scan results when Wifi scanning have finished
     *
     */
    private BroadcastReceiver wifiScanCompleteReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            long end_time = System.currentTimeMillis();
            long bootMS = System.currentTimeMillis() - SystemClock.elapsedRealtime();

            Intent dataCollectionIntent = new Intent(ACTION_WIFISCAN_BROADCAST);
            // empty previous stored wifiAP
            wifiAP.clear();
            List<ScanResult> results = wifiManager.getScanResults();

            // getScanResults may return null if scan failed
            if (results != null) {
                wifiAP.clear();
                for (ScanResult r : results) {
                    long resultTimestamp = 0;
                    resultTimestamp = getWifiTimestamp(r);
                    long timstampMS = resultTimestamp/1000;
                    long scan_time = bootMS + timstampMS;

                    readings.add(new WifiReading(r.BSSID, r.SSID, r.level, scan_time));
                    wifiAP.add(r.BSSID);
                    wifiAP.add(String.valueOf(r.level));
                }
                dataCollectionIntent.putStringArrayListExtra(EXTRA_WIFI_AP,wifiAP);
                WifiScan wifiScan = new WifiScan(0, end_time, readings);
                // add wifiScan to local db
                getDb().addWifiScan(wifiScan);
                readings.clear();
            } else {
                // send empty array list to ui
                dataCollectionIntent.putStringArrayListExtra(EXTRA_WIFI_AP,wifiAP);
            }
            intent.putExtra(EXTRA_WIFI_AP_TIME, System.currentTimeMillis());
            // send to ui
            LocalBroadcastManager.getInstance(context).sendBroadcast(dataCollectionIntent);


        }

    };

    @SuppressLint("NewApi")
    private long getWifiTimestamp(ScanResult result){
        return result.timestamp;
    }


    /**
    * Used to scan wifi every X interval executed by Handler
    */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            wifiManager.startScan();
            /* reschedule the runnable task to run in next time set in scanWifiInterval*/
            handler.postDelayed(this, scanWifiInterval);

        }
    };

    /**
     * Starts the sensor collection and the sensor data collected will be stored in database.
     * The sensor will store wifi AP BSSID, SSID, signal level, wifi AP scan time.
     */
    public void register(){
        // register receiver for Wifi scan complete
        IntentFilter wifiScanCompletefilter  = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        getContext().registerReceiver(wifiScanCompleteReceiver, wifiScanCompletefilter);

        // schedule wifi to scan every X interval
        handler.postDelayed(runnable, scanWifiInterval);
    }

    /**
     * Stops the wifi sensor collection.
     */
    public void unregister(){
        // remove receiver for receiving Broadcast from WifiManager.SCAN_RESULTS_AVAILABLE_ACTION
        getContext().unregisterReceiver(wifiScanCompleteReceiver);

        // stop runnable for executing wifiManager.startScan()
        handler.removeCallbacks(runnable);
    }

}