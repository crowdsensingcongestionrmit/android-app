package com.example.crowdsensing.sensorcollection.pushsensor;

import android.content.Context;

import com.example.crowdsensing.MySQLite;
import com.example.crowdsensing.sensorcollection.CollectSensor;

/**
 * This class should be extended by Sensor class which does not use SensorEventListener for
 * sensor readings updates.
 *
 * This class provides generic method for registering and unregistering sensor for collecting
 * sensor data.
 */
public abstract class PushSensor extends CollectSensor {

    private MySQLite db;
    private Context context;

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create Android SensorManager object to start the sensor
     *                 collection. If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    public PushSensor(Context context,MySQLite db){
        this.context = context;
        this.db = db;
    }

    /**
     * Returns the context used to create Sensor
     * <p>
     *
     * @return      the Context used in object construction parameter
     */
    public Context getContext() {
        return context;
    }

    /**
     * Returns the database used to create Sensor
     * <p>
     *
     * @return      the MySQLite object used in object construction parameter
     */
    public MySQLite getDb() {
        return db;
    }

    /**
     * Starts the sensor collection and the sensor data collected will be stored in database.
     */
    public abstract void register();

    /**
     * Stops the sensor collection.
     */
    public abstract void unregister();

}
