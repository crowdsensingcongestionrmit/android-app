package com.example.crowdsensing.sensorcollection.pushsensor;


import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.example.crowdsensing.MySQLite;

/**
 * This class is used for collecting nearby Bluetooth sensor data.
 * It extends the Android's PushSensor to starts and stops Bluetooth
 * sensor collection. The Bluetooth will scans for every 30s. It will starts Bluetooth LE scan when
 * Bluetooth scan is finished.
 */
public class BluetoothSensor extends PushSensor{

    private BluetoothAdapter mBtAdapter;
    private BroadcastReceiver mBtReceiver;
    //bluetooth state before BluetoothSensor is registered
    private boolean deviceBtStateEnabled;


    private IntentFilter deviceFoundFilter;
    private IntentFilter scanFinishFilter;

    private Handler handler = new Handler();
    private final int scanBluetoothInterval = 30000;

    private BluetoothAdapter.LeScanCallback mLeScanCallback;

    /**
     * Constructs an object that can be used to start and stop sensor collection.
     * <p>
     *
     * @param context  the context used to create and start the sensor collection.
     *                 If the context is null, the object constructed cannot be used to
     *                 start collection.
     *
     * @param db  the database object used to store the sensor data. If the context is null,
     *            the sensor object which uses the database instance will crash the application when
     *            trying to store the sensor data. All subclass of CollectSensor shall use the same
     *            database instance to store the sensor data.
     *
     */
    public BluetoothSensor(Context context, MySQLite db) {
        super(context,db);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mBtReceiver = new BluetoothFoundBroadcastReceiver();

        deviceFoundFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        scanFinishFilter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        if(mBtAdapter.isEnabled())
            deviceBtStateEnabled = true;
        else
            deviceBtStateEnabled = false;
    }

    /**
     * Starts the bluetooth and Bluetooth LE collection and the bluetooth data collected will be
     * stored in database. The Bluetooth will request device to be set discoverable mode for 300s
     * when sensor is registered. The sensor will store Nearby Bluetooth rssi, address, scan time.
     */
    @Override
    public void register() {
        if(mBtAdapter.isEnabled())
            deviceBtStateEnabled = true;
        else
            deviceBtStateEnabled = false;
        Log.d("BluetoothSensor", "register");

        getContext().registerReceiver(mBtReceiver, deviceFoundFilter);
        getContext().registerReceiver(mBtReceiver, scanFinishFilter);

        enableBluetooth();
        handler.postDelayed(bluetoothScanRunnable, scanBluetoothInterval);
        setDiscoverable();
    }

    /**
     * Stops the bluetooth collection and returns to the Bluetooth original state.
     */
    @Override
    public void unregister() {
        Log.d("BluetoothSensor","unregister");
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
            stopBluetoothLeScan();
        }

        // return to device Bluetooth state back to before BluetoothSensor is registered
        if(!deviceBtStateEnabled) {
            stopBluetoothLeScan();
            mBtAdapter.disable();
        }
        getContext().unregisterReceiver(mBtReceiver);
        handler.removeCallbacks(bluetoothScanRunnable);
    }

    /**
     * Sets the device bluetooth into discoverable mode
     */
    public void setDiscoverable(){
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        if(mBtAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            discoverableIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getContext().startActivity(discoverableIntent);
        }

    }

    /**
    * Used to scan bluetooth every X interval executed by Handler
    */
    private Runnable bluetoothScanRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("BluetoothSensor","Start Scan");
            stopBluetoothLeScan();
            mBtAdapter.startDiscovery();
            /* reschedule the runnable task to run in next time set in scanWifiInterval*/
            handler.postDelayed(this, scanBluetoothInterval);
        }
    };


    /**
     * Store nearby Bluetooth data into database and starts Bluetooth LE scan when Bluetooth scan is
     * finished.
     */
    private class BluetoothFoundBroadcastReceiver extends BroadcastReceiver{
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action))
            {
                long currentTime = System.currentTimeMillis();
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, (short) 0);
                getDb().addBluetoothReading(device.getAddress(),currentTime,rssi,false);
                getDb().addBluetoothReading(device.getAddress(),System.currentTimeMillis(),(short)rssi,false);
            }else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                // scans bt LE device when BT device scan is finished
                startBluetoothLeScan();
            }
        }
    }

    /* waits for Bluetooth to be enabled */
    private boolean enableBluetooth(){

        mBtAdapter.enable();
        while (!BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            try
            {
                Thread.sleep(100L);
            }
            catch (InterruptedException ie)
            {
                // unexpected interruption while enabling bluetooth
                Thread.currentThread().interrupt(); // restore interrupted flag
                return false;
            }
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void initBluetoothLeCallBack(){
        // Device scan callback.
        mLeScanCallback =
                new BluetoothAdapter.LeScanCallback() {
                    @Override
                    public void onLeScan(final BluetoothDevice device, int rssi,
                                         byte[] scanRecord) {
                        getDb().addBluetoothReading(device.getAddress(),System.currentTimeMillis(),(short)rssi,true);
                    }
                };

    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void startBluetoothLeScan(){
        initBluetoothLeCallBack();
        mBtAdapter.startLeScan(mLeScanCallback);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void stopBluetoothLeScan(){
        mBtAdapter.stopLeScan(mLeScanCallback);
    }
}
