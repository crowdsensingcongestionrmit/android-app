package com.example.crowdsensing.sensorcollection;

/**
* This interface provides generic method for registering and unregistering sensor for collecting
* sensor data.
*/
public interface CollectSensorInterface {

    /**
    * Starts sensor collection
    */
    public void register();

    /**
    * Stops sensor sensor collection
    */
    public void unregister();
}
