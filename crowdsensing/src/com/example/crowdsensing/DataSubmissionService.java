package com.example.crowdsensing;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DataSubmissionService extends Service{
	
	private final String POST_URL = "http://115.146.85.116/v1/get-device-key";
	private final String TEST_URL = "http://coreteaching01.csit.rmit.edu.au/~s3105178/sep/v1/get-device-key.cgi";
	private final String STORE_URL = "http://coreteaching01.csit.rmit.edu.au/~s3105178/sep/v1/store.cgi";
	private MySQLite db;
	private final int retriveRowNo = 50;
	private String key;
	@Override
	public void onCreate() {
		Log.d("DataSubmission","onCreate");
		// TODO Auto-generated method stub
		super.onCreate();
		db = new MySQLite(this);
		
		SharedPreferences pref = getSharedPreferences("com.example.crowdsensing", MODE_PRIVATE);		
		key = pref.getString("com.example.crowdsensing.key", null);
		
		if(key == null){
			try {
				key = new WebServiceGetKey().execute().get(5000, TimeUnit.MILLISECONDS);
				Log.d("DataSubmission","key retrieved"+key);
				pref.edit().putString("com.example.crowdsensing.key", key);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.d("DataSubmission", "onStartCommand");

		Log.d("DataSubmission", "use key to post data start");
		if (this.isConnectedWifi()) {
			Log.d("DataSubmission", "use key to post data start");
			this.post_data(key);
		}
		// delete_data();

		Log.d("DataSubmission", "onStartCommand ends");

		this.stopSelf();
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private boolean post_data(String key){
		Log.d("DataSubmission","post data gps retrieve");
		List<GPSReading> gpsSet = db.getGPSReading(retriveRowNo);
		Log.d("DataSubmission","post data wifi retrieve");
		List<WifiScan> wifiSet = db.getWiFiScans(retriveRowNo);
		Log.d("DataSubmission","post data start with key"+key);
		long gpsMaxTime = 0;
		for(GPSReading current : gpsSet){
			if(current.getTime() > gpsMaxTime){
				gpsMaxTime = current.getTime();
			}
		}
		
		long wifiMaxTime = 0;
		for(WifiScan current : wifiSet){
			if(current.getEndTime() > wifiMaxTime){
				wifiMaxTime = current.getEndTime();
			}
		}
		
		try {
			JSONObject gpsJSON = this.buildLocationJSON(gpsSet);
			JSONObject wifiJSON = this.buildWifiAPJSON(wifiSet);
			String gpsSuccess = null;
			String wifiSuccess = null;
			try {
				Log.d("DataSubmission","posting to webserver start");
				gpsSuccess = new WebServiceStore().execute(key,gpsJSON).get();
				Log.d("DataSubmission","gps status : "+gpsSuccess);
				wifiSuccess = new WebServiceStore().execute(key,wifiJSON).get();
				Log.d("DataSubmission","wifi status : "+wifiSuccess);
				if(gpsSuccess == null || wifiSuccess == null){
					return false;
				}
				if(gpsSuccess != null){
					db.deleteGPSReadingsUntil(gpsMaxTime);
				} 
				if(wifiSuccess != null){
					//db.deleteWifiScanUntil(wifiMaxTime);
				}
				Log.d("DataSubmission","posting to webserver end success");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			
		} catch (JSONException e) {
			return false;
		}
		
		Log.d("DataSubmission","post data end");
		return true;
	}

	private boolean delete_data(){
		List<GPSReading> gpsSet = db.getGPSReading(retriveRowNo);
		List<WifiScan> wifiSet = db.getWiFiScans(retriveRowNo);
		Log.d("DataSubmission","retrieve_data");
		long gpsMaxTime = 0;
		for(GPSReading current : gpsSet){
			if(current.getTime() > gpsMaxTime){
				gpsMaxTime = current.getTime();
			}
		}
		
		long wifiMaxTime = 0;
		for(WifiScan current : wifiSet){
			if(current.getEndTime() > wifiMaxTime){
				wifiMaxTime = current.getEndTime();
			}
		}
		Log.d("DataSubmission","GPSReading "+gpsSet.size());
		Log.d("DataSubmission","WifiScan "+wifiSet.size());
		Log.d("DataSubmission","retrieve_data end");
		Log.d("DataSubmission","delete_data");
		db.deleteGPSReadingsUntil(gpsMaxTime);
		Log.d("DataSubmissionService","delete_data gps with "+gpsMaxTime);
		db.deleteWiFiScansUntil(wifiMaxTime);
		Log.d("DataSubmissionService","delete_data wifi "+wifiMaxTime);
		
		return true;
	}	
	
	private JSONObject buildWifiAPJSON(Collection<WifiScan> wifiScanSet) throws JSONException{
		JSONObject wifiScanSetJSON = new JSONObject();
		JSONArray wifiScanArray = new JSONArray();
		
		for(WifiScan wifiScan : wifiScanSet){
			JSONObject wifiScanJSON = new JSONObject();
			JSONArray wifiScanReadingArray = new JSONArray();
			wifiScanJSON.put("ScanStartTime", wifiScan.getStartTime());
			wifiScanJSON.put("ScanEndTime", wifiScan.getEndTime());
			
			
			for(WifiReading r : wifiScan.getReadings()){
				JSONObject readings = new JSONObject();
				readings.put("ScanTime", r.getTime());
				readings.put("MacAddress", r.getMacAddress());
				readings.put("NetworkName", r.getName());
				readings.put("Signal", r.getSignalLevel());
				wifiScanReadingArray.put(readings);
			}
			wifiScanJSON.put("WifiReading", wifiScanReadingArray);
			JSONObject wifiScanContainer = new JSONObject();
			wifiScanContainer.put("WifiScan", wifiScanJSON);
			
			wifiScanArray.put(wifiScanContainer);
		}
		wifiScanSetJSON .put("WifiScans",wifiScanArray);
		return wifiScanSetJSON ;
	}
	
	private JSONObject buildLocationJSON(Collection<GPSReading> gps) throws JSONException{
		JSONObject locationScan = new JSONObject();
		JSONArray locationArray = new JSONArray();
		
		for(GPSReading r : gps){
			JSONObject readings = new JSONObject();
			readings.put("ScanTime", r.getTime());
			readings.put("Longitude", r.getLongitude());
			readings.put("Latitude", r.getLatitude());
			readings.put("Accuracy", r.getAccuracy());
			locationArray.put(readings);
		}

		locationScan.put("GPSReadings", locationArray);
		
		return locationScan;
	}
	
	
    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
    	BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
    	 StringBuilder total = new StringBuilder();
    	 String line;
    	  while ((line = r.readLine()) != null) {
    	    total.append(line);
    	  }
    	return total.toString();
 
    }
    
	public boolean isConnectedWifi() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI && networkInfo.isConnected())
			return true;
		else
			return false;
	}

	class WebServiceGetKey extends AsyncTask <String, Integer, String>{
		
		private String requestDeviceKey() throws ClientProtocolException, IOException{
			Log.d("DataSubmission", "WebServiceGetKeyAsyncTask start");
			InputStream inputStream = null;
			String postResult = null;		
			
			// 1. create HttpClient
	        HttpClient httpclient = new DefaultHttpClient();

	        // 2. make POST request to the given URL
	        HttpPost httpPost = new HttpPost(TEST_URL);
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("secret", "12345678"));

	        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
	        HttpResponse httpResponse = httpclient.execute(httpPost);
	        if(httpResponse.getStatusLine().getStatusCode() == 200){
	        	Log.d("DataSubmission","Return code 200");
	        	
		        // 3. receive response as inputStream
		        inputStream = httpResponse.getEntity().getContent();

		        // 4. convert inputstream to string
		        if(inputStream != null){
		        	// assign secret key
		        	postResult = convertInputStreamToString(inputStream);
		        	Log.d("DataSubmission","Secret key "+postResult);
		        	}
		        else
		        	postResult = null;
	        
	        }else{
	        	Log.d("DataSubmission","get seccret key failed");
	        }
	        
	        Log.d("DataSubmission", "WebServiceGetKeyAsyncTask ends "+postResult);
			return postResult;
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				String key = requestDeviceKey();
				Log.d("Request Key", key);
				return key;
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	}
	
	class WebServiceStore extends AsyncTask <Object, Integer, String>{
		
		private String store(String key,JSONObject postJSON) throws ClientProtocolException, IOException{

			InputStream inputStream = null;
			String postResult = null;		
			Log.d("DataSubmission","store start : "+postResult);
			// 1. create HttpClient
	        HttpClient httpclient = new DefaultHttpClient();

	        // 2. make POST request to the given URL
	        HttpPost httpPost = new HttpPost(STORE_URL);
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        nameValuePairs.add(new BasicNameValuePair("key", key));
	        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
	        // 3. convert JSONObject to JSON to String
	        Log.d("GCFinder","GCFinder 1");
	        String jsonString = postJSON.toString();
	        Log.d("GCFinder","GCFinder 2");
	        nameValuePairs.add(new BasicNameValuePair("data", jsonString));
	        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
	        // 4. Execute POST request to the given URL
	        HttpResponse httpResponse = httpclient.execute(httpPost);
	        if(httpResponse.getStatusLine().getStatusCode() == 200){
		        // 5. receive response as inputStream
		        inputStream = httpResponse.getEntity().getContent();

		        // 9. convert inputstream to string
		        if(inputStream != null){
		        	postResult = "";//convertInputStreamToString(inputStream);
		        	Log.d("DataSubmission","PostResult "+postResult);
		        	}
		        else
		        	postResult = null;
	        
	        
			}
	        Log.d("DataSubmission","store end : "+postResult);
			return postResult;
		}

		@Override
		protected String doInBackground(Object... params) {
			// TODO Auto-generated method stub
			String key = params[0].toString();
			JSONObject postJSON = (JSONObject) params[1];
			
			try {
				return store(key,postJSON);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	}	


}
