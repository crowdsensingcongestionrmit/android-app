package com.example.crowdsensing;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.crowdsensing.sensorreading.AccelerometerReading;
import com.example.crowdsensing.sensorreading.BarometerReading;
import com.example.crowdsensing.sensorreading.BluetoothReading;
import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.MagnetometerReading;
import com.example.crowdsensing.sensorreading.TemperatureReading;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MySQLite extends SQLiteOpenHelper implements MySQLiteInterface{
	
	// Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "PositionDB";
 
    public MySQLite(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);  
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create position table
        String CREATE_POSITION_TABLE = "CREATE TABLE positions ( " +
                "longitude DOUBLE, "+
                "latitude DOUBLE, "+
                "accuracy DOUBLE, "+
                "time INTEGER )";
 
        // create positions table
    	
    	db.execSQL(CREATE_POSITION_TABLE);
    	
    	String CREATE_WIFISCAN_TABLE = "CREATE TABLE wifiscans ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, "+
                "starttime INTEGER, "+
                "endtime INTEGER )";
    	
    	db.execSQL(CREATE_WIFISCAN_TABLE);
    	
    	String CREATE_WIFIREADING_TABLE = "CREATE TABLE wifireadings ( " +
                "scanid INTEGER, "+
                "macaddress TEXT, "+
                "ssid TEXT, "+
                "signallevel INTEGER, "+
                "time INTEGER )";
    	
    	db.execSQL(CREATE_WIFIREADING_TABLE);
    	
    	String CREATE_ACCELEROMETER_TABLE = "CREATE TABLE accelerometerreadings ( " +
                "time INTEGER, "+
                "xvalue DOUBLE, "+
                "yvalue DOUBLE, "+
                "zvalue DOUBLE )";
    	
    	db.execSQL(CREATE_ACCELEROMETER_TABLE);
    	
    	String CREATE_MAGNETOMETER_TABLE = "CREATE TABLE magnetometerreadings ( " +
    			"time INTEGER, "+
                "xvalue DOUBLE, "+
                "yvalue DOUBLE, "+
                "zvalue DOUBLE )";
    	
    	db.execSQL(CREATE_MAGNETOMETER_TABLE);
    	
    	String CREATE_BAROMETER_TABLE = "CREATE TABLE barometerreadings ( " +
                "time INTEGER, "+
                "pressure DOUBLE )";
    	
    	db.execSQL(CREATE_BAROMETER_TABLE);
    	
    	String CREATE_TEMPERATURE_TABLE = "CREATE TABLE temperaturereadings ( " +
    			"time INTEGER, "+
                "degree DOUBLE )";
    	
    	db.execSQL(CREATE_TEMPERATURE_TABLE);
    	
    	String CREATE_TEXTEVENT_TABLE = "CREATE TABLE textevents ( " +
    			"time INTEGER, "+
                "label TEXT NULLABLE )";
    	
    	db.execSQL(CREATE_TEXTEVENT_TABLE);

        String CREATE_BLUETOOTH_TABLE = "CREATE TABLE bluetooths ( " +
                "id TEXT, "+
                "rssi INTEGER, "+
                "isletype INTEGER, "+
                "time INTEGER )";

        db.execSQL(CREATE_BLUETOOTH_TABLE);

        String CREATE_CELLTOWER_TABLE = "CREATE TABLE celltower ( " +
                "id INTEGER, "+
                "mcc INTEGER, "+
                "time INTEGER, "+
                "mnc INTEGER, "+
                "lac INTEGER )";

        db.execSQL(CREATE_CELLTOWER_TABLE);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS positions");
        db.execSQL("DROP TABLE IF EXISTS wifiscans");
        db.execSQL("DROP TABLE IF EXISTS wifireadings");
        db.execSQL("DROP TABLE IF EXISTS accelerometerreadings");
        db.execSQL("DROP TABLE IF EXISTS magnetometerreadings");
        db.execSQL("DROP TABLE IF EXISTS barometerreadings");
        db.execSQL("DROP TABLE IF EXISTS temperaturereadings");
        db.execSQL("DROP TABLE IF EXISTS textevents");
        db.execSQL("DROP TABLE IF EXISTS bluetooths");
        db.execSQL("DROP TABLE IF EXISTS celltower");
        // create fresh books table
        this.onCreate(db);
    }
    //---------------------------------------------------------------------
 
    /**
     * CRUD operations (create "add", read "get", update, delete) position + get all positions + delete all positions
     */
 
    // Positions table name
    private static final String TABLE_POSITIONS = "positions";
    private static final String TABLE_WIFISCAN = "wifiscans";
    private static final String TABLE_WIFIREADING = "wifireadings";
    private static final String TABLE_ACCELEROMETER = "accelerometerreadings";
    private static final String TABLE_MAGNETOMETER = "magnetometerreadings";
    private static final String TABLE_BAROMETER = "barometerreadings";
    private static final String TABLE_TEMPERATURE = "temperaturereadings";
    private static final String TABLE_TEXTEVENTS = "textevents";
    private static final String TABLE_BLUETOOTH = "bluetooths";
    private static final String TABLE_CELLTOWER = "celltower";
 
    // Positions Table Columns names
    private static final String POSITION_LONGITUDE = "longitude";
    private static final String POSITION_LATITUDE = "latitude";
    private static final String POSITION_ACCURACY = "accuracy";
    private static final String POSITION_TIME = "time";
    
    private static final String WIFISCAN_ID = "id";
    private static final String WIFISCAN_STARTTIME = "starttime";
    private static final String WIFISCAN_ENDTIME = "endtime";
    
    private static final String WIFIREADING_SCANID = "scanid";
    private static final String WIFIREADING_MACADDRESS = "macaddress";
    private static final String WIFIREADING_SSID = "ssid";
    private static final String WIFIREADING_SIGNALLEVEL = "signallevel";
    private static final String WIFIREADING_TIME = "time";
    
    private static final String ACCELEROMETER_TIME = "time";
    private static final String ACCELEROMETER_XVALUE = "xvalue";
    private static final String ACCELEROMETER_YVALUE = "yvalue";
    private static final String ACCELEROMETER_ZVALUE = "zvalue";
    
    private static final String MAGNETOMETER_TIME = "time";
    private static final String MAGNETOMETER_XVALUE = "xvalue";
    private static final String MAGNETOMETER_YVALUE = "yvalue";
    private static final String MAGNETOMETER_ZVALUE = "zvalue";
    
    private static final String BAROMETER_TIME = "time";
    private static final String BAROMETER_PRESSURE = "pressure";
    
    private static final String TEMPERATURE_TIME = "time";
    private static final String TEMPERATURE_DEGREE = "degree";
    
    private static final String TEXTEVENTS_TIME = "time";
    private static final String TEXTEVENTS_LABEL = "label";

    private static final String BLUETOOTH_ID = "id";
    private static final String BLUETOOTH_TIME = "time";
    private static final String BLUETOOTH_RSSI = "rssi";
    private static final String BLUETOOTH_TYPE = "isletype";

    private static final String CELL_MCC = "mcc";
    private static final String CELL_MNC = "mnc";
    private static final String CELL_ID = "id";
    private static final String CELL_LAC = "lac";
    private static final String CELL_TIME = "time";

    private ArrayList<AccelerometerReading> tempAccelerometer = new ArrayList<AccelerometerReading>();

    public void addGPSReading(GPSReading position){
//        Log.d("addPosition", position.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(POSITION_LONGITUDE, position.getLongitude());
        values.put(POSITION_LATITUDE, position.getLatitude());
        values.put(POSITION_ACCURACY, position.getAccuracy());
        values.put(POSITION_TIME, position.getTime()); 
 
        // 3. insert
        db.insert(TABLE_POSITIONS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addGPSReading(double longi,double lati,float accuracy,long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(POSITION_LONGITUDE, longi);
        values.put(POSITION_LATITUDE, lati);
        values.put(POSITION_ACCURACY, accuracy);
        values.put(POSITION_TIME, time); 
 
        // 3. insert
        db.insert(TABLE_POSITIONS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addWifiScan(WifiScan scan){
//    	Log.d("addWifiScan", scan.toString());
    	 // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(WIFISCAN_STARTTIME, scan.getStartTime()); 
        values.put(WIFISCAN_ENDTIME, scan.getEndTime()); 
 
        db.beginTransaction();
        try {
            // 3. insert
            long rowID = db.insert(TABLE_WIFISCAN, // table
                    null, //nullColumnHack
                    values); // key/value -> keys = column names/ values = column values

            for (WifiReading reading : scan.getReadings()) {
//                Log.d("addWifiReading", reading.toString());

                // 2. create ContentValues to add key "column"/value
                ContentValues values1 = new ContentValues();
                values1.put(WIFIREADING_SCANID, rowID);
                values1.put(WIFIREADING_MACADDRESS, reading.getMacAddress());
                values1.put(WIFIREADING_SSID, reading.getName());
                values1.put(WIFIREADING_SIGNALLEVEL, reading.getSignalLevel());
                values1.put(WIFIREADING_TIME, reading.getTime());

                // 3. insert
                db.insert(TABLE_WIFIREADING, // table
                    null, //nullColumnHack
                    values1); // key/value -> keys = column names/ values = column values
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // 4. close
        db.close();
    }
    
    public void addWifiScan(long starttime,long endtime,List<WifiReading> readings){
    	 // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(WIFISCAN_STARTTIME, starttime); 
        values.put(WIFISCAN_ENDTIME, endtime); 
 
        db.beginTransaction();
        try {
            // 3. insert
            long rowID = db.insert(TABLE_WIFISCAN, // table
                    null, //nullColumnHack
                    values); // key/value -> keys = column names/ values = column values

            for (WifiReading reading : readings) {
//                Log.d("addWifiReading", reading.toString());

                // 2. create ContentValues to add key "column"/value
                ContentValues values1 = new ContentValues();
                values1.put(WIFIREADING_SCANID, rowID);
                values1.put(WIFIREADING_MACADDRESS, reading.getMacAddress());
                values1.put(WIFIREADING_SSID, reading.getName());
                values1.put(WIFIREADING_SIGNALLEVEL, reading.getSignalLevel());
                values1.put(WIFIREADING_TIME, reading.getTime());

                // 3. insert
                db.insert(TABLE_WIFIREADING, // table
                    null, //nullColumnHack
                    values1); // key/value -> keys = column names/ values = column values
            }
            db.setTransactionSuccessful();
        }
        finally {
            db.endTransaction();
        }

        // 4. close
        db.close();
    }
    
    public void addAccelerometerReading(AccelerometerReading accelerometer){
//        Log.d("addAccelerometer", accelerometer.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(ACCELEROMETER_TIME, accelerometer.getTime());
        values.put(ACCELEROMETER_XVALUE, accelerometer.getXValue());
        values.put(ACCELEROMETER_YVALUE, accelerometer.getYValue());
        values.put(ACCELEROMETER_ZVALUE, accelerometer.getZValue()); 
 
        // 3. insert
        db.insert(TABLE_ACCELEROMETER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addAccelerometerReading(long time, float xvalue, float yvalue, float zvalue){
        tempAccelerometer.add(new AccelerometerReading(time,xvalue,yvalue,zvalue));

        if(tempAccelerometer.size() == 100) {
            // 1. get reference to writable DB
            SQLiteDatabase db = this.getWritableDatabase();

            db.beginTransaction();
            try {
                for (int i = 0; i < tempAccelerometer.size(); i++) {
                    // 2. create ContentValues to add key "column"/value
                    ContentValues values = new ContentValues();
                    values.put(ACCELEROMETER_TIME, tempAccelerometer.get(i).getTime());
                    values.put(ACCELEROMETER_XVALUE, tempAccelerometer.get(i).getXValue());
                    values.put(ACCELEROMETER_YVALUE, tempAccelerometer.get(i).getYValue());
                    values.put(ACCELEROMETER_ZVALUE, tempAccelerometer.get(i).getZValue());

                    // 3. insert
                    db.insert(TABLE_ACCELEROMETER, // table
                            null, //nullColumnHack
                            values); // key/value -> keys = column names/ values = column values
                }
                db.setTransactionSuccessful();
                tempAccelerometer.clear();
            }finally {
                db.endTransaction();
            }
            // 4. close
            db.close();
        }
    }
    
    public void addMagenetometerReading(MagnetometerReading magnetometer){
//        Log.d("addMagnetometer", magnetometer.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(MAGNETOMETER_TIME, magnetometer.getTime());
        values.put(MAGNETOMETER_XVALUE, magnetometer.getXValue());
        values.put(MAGNETOMETER_YVALUE, magnetometer.getYValue());
        values.put(MAGNETOMETER_ZVALUE, magnetometer.getZValue()); 
 
        // 3. insert
        db.insert(TABLE_MAGNETOMETER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addMagenetometerReading(long time, float xvalue, float yvalue, float zvalue){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(MAGNETOMETER_TIME, time);
        values.put(MAGNETOMETER_XVALUE,xvalue);
        values.put(MAGNETOMETER_YVALUE, yvalue);
        values.put(MAGNETOMETER_ZVALUE, zvalue); 
 
        // 3. insert
        db.insert(TABLE_MAGNETOMETER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addBarometerReading(BarometerReading barometer){
//        Log.d("addBarometer", barometer.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(BAROMETER_TIME, barometer.getTime());
        values.put(BAROMETER_PRESSURE, barometer.getPressure());
 
        // 3. insert
        db.insert(TABLE_BAROMETER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addBarometerReading(long time, float pressure){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(BAROMETER_TIME, time);
        values.put(BAROMETER_PRESSURE, pressure);
 
        // 3. insert
        db.insert(TABLE_BAROMETER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addTemperatureReading(TemperatureReading temperature){
//        Log.d("addTemperature", temperature.toString());
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(TEMPERATURE_TIME, temperature.getTime());
        values.put(TEMPERATURE_DEGREE, temperature.getDegreesCelcius());
 
        // 3. insert
        db.insert(TABLE_TEMPERATURE, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
 
    public void addTemperatureReading(long time, float degreeCelcius){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(TEMPERATURE_TIME, time);
        values.put(TEMPERATURE_DEGREE, degreeCelcius);
 
        // 3. insert
        db.insert(TABLE_TEMPERATURE, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    public void addTextEvent(String text){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(TEXTEVENTS_TIME, System.currentTimeMillis());
        values.put(TEXTEVENTS_LABEL, text);
 
        // 3. insert
        db.insert(TABLE_TEXTEVENTS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values
 
        // 4. close
        db.close(); 
    }
    
    /**
     * Retrieve GPS readings from the device-local database.
     *
     * @param limit
     *   The maximum number of readings to retrieve.  Oldest GPS
     *   readings are given preference.
     */
    public List<GPSReading> getGPSReading(int limit) {
    	
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_POSITIONS,
            new String[] {
                POSITION_LONGITUDE,
                POSITION_LATITUDE,
                POSITION_ACCURACY,
                POSITION_TIME
            },
            null,
            null,
            null,
            null,
            POSITION_TIME + " ASC",
            String.valueOf(limit)
        );
        List<GPSReading> positions = new ArrayList<GPSReading>(limit);
        while (cursor.moveToNext()) {
            positions.add(new GPSReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getDouble(0),
                cursor.getDouble(1),
                cursor.getFloat(2),
                cursor.getLong(3)
            ));
        }
 
//        Log.d("getAllPositions()", positions.toString());
        
        db.close();
        return positions;
    }
    
    public JSONObject getGPSReadingJSON(int limit) throws JSONException {
    	JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
    	
        Cursor cursor = db.query(
            TABLE_POSITIONS,
            new String[] {
                POSITION_LONGITUDE,
                POSITION_LATITUDE,
                POSITION_ACCURACY,
                POSITION_TIME
            },
            null,
            null,
            null,
            null,
            POSITION_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject readings = new JSONObject();
            readings.put("ScanTime", cursor.getLong(3));
            readings.put("Longitude", cursor.getDouble(0));
            readings.put("Latitude", cursor.getDouble(1));
            readings.put("Accuracy", cursor.getFloat(2));
            locationArray.put(readings);
        }
        db.close();
        locationScan.put("GPSReadings", locationArray);
        return locationScan;
    }
    
    /**
     * Remove all GPS readings for a specific time or earlier from the
     * device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such readings.
     *
     * @param time
     *    The UTC time of the newest GPS reading that should be removed
     *    from the device-local database, in milliseconds since 1
     *    January 1970.
     */
    public void deleteGPSReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(
            TABLE_POSITIONS,
            POSITION_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
    	db.close();
    }
    
    /**
     * Retrieve Wi-Fi scan records from the device-local database.
     *
     * @param limit
     *   The maximum number of records to retrieve.  Oldest Wi-Fi scans
     *   are given preference.
     */
    public List<WifiScan> getWiFiScans(int limit) {
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
            TABLE_WIFISCAN,
            new String[] {
                WIFISCAN_ID,
                WIFISCAN_STARTTIME,
                WIFISCAN_ENDTIME
            },
            null,
            null,
            null,
            null,
            WIFISCAN_ID + " ASC",
            String.valueOf(limit)
        );
        List<WifiScan> scans = new ArrayList<WifiScan>(limit);
        while (cursor.moveToNext()) {
            scans.add(new WifiScan(
                cursor.getLong(1),
                cursor.getLong(2),
                getWiFiReadings(cursor.getInt(0))
            ));
        }
 
//        Log.d("getWiFiScans()", scans.toString());
        db.close();
        return scans;
    }
    
    public JSONObject getWiFiScansJSON(int limit) throws JSONException {
    	JSONObject wifiScanSetJSON = new JSONObject();
        JSONArray wifiScanArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
            TABLE_WIFISCAN,
            new String[] {
                WIFISCAN_ID,
                WIFISCAN_STARTTIME,
                WIFISCAN_ENDTIME
            },
            null,
            null,
            null,
            null,
            WIFISCAN_ID + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject wifiScanJSON = new JSONObject();
            wifiScanJSON.put("ScanStartTime", cursor.getLong(1));
            wifiScanJSON.put("ScanEndTime", cursor.getLong(2));
            wifiScanJSON.put("WifiReading", getWiFiReadingsJSON(cursor.getInt(0)));
            JSONObject wifiScanContainer = new JSONObject();
            wifiScanContainer.put("WifiScan", wifiScanJSON);

            wifiScanArray.put(wifiScanContainer);
        }
        db.close();
        wifiScanSetJSON .put("WifiScans",wifiScanArray);
        return wifiScanSetJSON;
    }

    /**
     * Remove all Wi-Fi scan records for a specific time or earlier
     * from the device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such records.
     *
     * @param endTime
     *    The UTC end time of the newest Wi-Fi scan record that should
     *    be removed from the device-local database, in milliseconds
     *    since 1 January 1970.
     */
	public void deleteWiFiScansUntil(long endTime)
    {
        SQLiteDatabase _db = this.getWritableDatabase();
        _db.beginTransaction();
        try {
            _db.execSQL(String.format(
                "DELETE FROM %s WHERE %s IN"
                    + " (SELECT %s FROM %s WHERE %s <= %d)",
                TABLE_WIFIREADING,
                WIFIREADING_SCANID,
                WIFISCAN_ID,
                TABLE_WIFISCAN,
                WIFISCAN_ENDTIME,
                endTime
            ));
            _db.delete(
                TABLE_WIFISCAN,
                WIFISCAN_ENDTIME + " <= ?",
                new String[] { String.valueOf(endTime) }
            );
            _db.setTransactionSuccessful();
        }
        finally {
            _db.endTransaction();
        }
        _db.close();
    }
	
	private List<WifiReading> getWiFiReadings(int scanId) {
		SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
            TABLE_WIFIREADING,
            new String[] {
                WIFIREADING_MACADDRESS,
                WIFIREADING_SSID,
                WIFIREADING_SIGNALLEVEL,
                WIFIREADING_TIME,
            },
            WIFIREADING_SCANID + " = ?",
            new String[] { String.valueOf(scanId) },
            null,
            null,
            null
        );
        List<WifiReading> readings = new ArrayList<WifiReading>();
        while (cursor.moveToNext()) {
            readings.add(new WifiReading(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getInt(2),
                cursor.getLong(3)
            ));
        }
        db.close();
		return readings;
	}
	
	private JSONArray getWiFiReadingsJSON(int scanId) throws JSONException {
		JSONArray wifiScanReadingArray = new JSONArray();
		SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
            TABLE_WIFIREADING,
            new String[] {
                WIFIREADING_MACADDRESS,
                WIFIREADING_SSID,
                WIFIREADING_SIGNALLEVEL,
                WIFIREADING_TIME,
            },
            WIFIREADING_SCANID + " = ?",
            new String[] { String.valueOf(scanId) },
            null,
            null,
            null
        );
        while (cursor.moveToNext()) {
        	JSONObject readings = new JSONObject();
        	readings.put("ScanTime", cursor.getLong(3));
            readings.put("MacAddress",cursor.getString(0));
            readings.put("NetworkName", cursor.getString(1));
            readings.put("Signal", cursor.getInt(2));
            wifiScanReadingArray.put(readings);
        }
        db.close();
		return wifiScanReadingArray;
	}
	
	/**
     * Retrieve Accelerometer readings from the device-local database.
     *
     * @param limit
     *   The maximum number of readings to retrieve.  Oldest accelerometer
     *   readings are given preference.
     */
    public List<AccelerometerReading> getAccelerometerReading(int limit) {
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_ACCELEROMETER,
            new String[] {
        		ACCELEROMETER_TIME,
        		ACCELEROMETER_XVALUE,
        		ACCELEROMETER_YVALUE,
        		ACCELEROMETER_ZVALUE
            },
            null,
            null,
            null,
            null,
            ACCELEROMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        List<AccelerometerReading> accelerometers = new ArrayList<AccelerometerReading>(limit);
        while (cursor.moveToNext()) {
        	accelerometers.add(new AccelerometerReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getLong(0),
                cursor.getFloat(1),
                cursor.getFloat(2),
                cursor.getFloat(3)
            ));
        }
 
//        Log.d("getAllPositions()", accelerometers.toString());
        db.close();
        return accelerometers;
    }
    
    public JSONObject getAccelerometerReadingJSON(int limit) throws JSONException{
    	JSONObject accelerometer = new JSONObject();
        JSONArray accelerometerArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_ACCELEROMETER,
            new String[] {
        		ACCELEROMETER_TIME,
        		ACCELEROMETER_XVALUE,
        		ACCELEROMETER_YVALUE,
        		ACCELEROMETER_ZVALUE
            },
            null,
            null,
            null,
            null,
            ACCELEROMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	
        	JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("x",cursor.getFloat(1));
            reading.put("y",cursor.getFloat(2));
            reading.put("z",cursor.getFloat(3));
            accelerometerArray.put(reading);
        }
        db.close();
        accelerometer.put("Accelerometer",accelerometerArray);
        return accelerometer;
    }
    
    /**
     * Remove all accelerometer readings for a specific time or earlier from the
     * device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such readings.
     *
     * @param time
     *    The UTC time of the newest accelerometer reading that should be removed
     *    from the device-local database, in milliseconds since 1
     *    January 1970.
     */
    public void deleteAccelerometerReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_ACCELEROMETER,
        	ACCELEROMETER_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }
    
    /**
     * Retrieve Magnetometer readings from the device-local database.
     *
     * @param limit
     *   The maximum number of readings to retrieve.  Oldest magnetometer
     *   readings are given preference.
     */
    public List<MagnetometerReading> getMagnetometerReading(int limit) {
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_MAGNETOMETER,
            new String[] {
        		MAGNETOMETER_TIME,
        		MAGNETOMETER_XVALUE,
        		MAGNETOMETER_YVALUE,
        		MAGNETOMETER_ZVALUE
            },
            null,
            null,
            null,
            null,
            MAGNETOMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        List<MagnetometerReading> magnetometers = new ArrayList<MagnetometerReading>(limit);
        while (cursor.moveToNext()) {
        	magnetometers.add(new MagnetometerReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getLong(0),
                cursor.getFloat(1),
                cursor.getFloat(2),
                cursor.getFloat(3)
            ));
        }
 
//        Log.d("getAllPositions()", magnetometers.toString());
        db.close();
        return magnetometers;
    }
    
    public JSONObject getMagnetometerReadingJSON(int limit) throws JSONException {
    	JSONObject magnetometer = new JSONObject();
        JSONArray magnetometerArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_MAGNETOMETER,
            new String[] {
        		MAGNETOMETER_TIME,
        		MAGNETOMETER_XVALUE,
        		MAGNETOMETER_YVALUE,
        		MAGNETOMETER_ZVALUE
            },
            null,
            null,
            null,
            null,
            MAGNETOMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("x",cursor.getFloat(1));
            reading.put("y",cursor.getFloat(2));
            reading.put("z",cursor.getFloat(3));
            magnetometerArray.put(reading);
        }
        db.close();
        magnetometer.put("Magnetometer",magnetometerArray);
        return magnetometer;
    }
    
    /**
     * Remove all magnetometer readings for a specific time or earlier from the
     * device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such readings.
     *
     * @param time
     *    The UTC time of the newest magnetometer reading that should be removed
     *    from the device-local database, in milliseconds since 1
     *    January 1970.
     */
    public void deleteMagnetometerReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_MAGNETOMETER,
        	MAGNETOMETER_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }
    
    /**
     * Retrieve Barometer readings from the device-local database.
     *
     * @param limit
     *   The maximum number of readings to retrieve.  Oldest barometer
     *   readings are given preference.
     */
    public List<BarometerReading> getBarometerReading(int limit) {
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_BAROMETER,
            new String[] {
        		BAROMETER_TIME,
        		BAROMETER_PRESSURE
            },
            null,
            null,
            null,
            null,
            BAROMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        List<BarometerReading> barometers = new ArrayList<BarometerReading>(limit);
        while (cursor.moveToNext()) {
        	barometers.add(new BarometerReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getLong(0),
                cursor.getFloat(1)
            ));
        }
 
//        Log.d("getAllPositions()", barometers.toString());
        db.close();
        return barometers;
    }
    
    public JSONObject getBarometerReadingJSON(int limit) throws JSONException {
    	JSONObject barometer = new JSONObject();
        JSONArray barometerArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_BAROMETER,
            new String[] {
        		BAROMETER_TIME,
        		BAROMETER_PRESSURE
            },
            null,
            null,
            null,
            null,
            BAROMETER_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject reading = new JSONObject();
            reading.put("timestamp", cursor.getLong(0));
            reading.put("pressure",cursor.getFloat(1));
            barometerArray.put(reading);
//        	barometers.add(new BarometerReading(
//                // These numeric indexes relate to the column order
//                // defined in the query() call above.
//                cursor.getLong(0),
//                cursor.getFloat(1)
//            ));
        }
        db.close();
        barometer.put("Barometer",barometerArray);
        return barometer;
    }
    
    /**
     * Remove all barometer readings for a specific time or earlier from the
     * device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such readings.
     *
     * @param time
     *    The UTC time of the newest barometer reading that should be removed
     *    from the device-local database, in milliseconds since 1
     *    January 1970.
     */
    public void deleteBarometerReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_BAROMETER,
        	BAROMETER_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }
    
    /**
     * Retrieve temperature readings from the device-local database.
     *
     * @param limit
     *   The maximum number of readings to retrieve.  Oldest temperature
     *   readings are given preference.
     */
    public List<TemperatureReading> getTemperatureReading(int limit) {
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_TEMPERATURE,
            new String[] {
        		TEMPERATURE_TIME,
        		TEMPERATURE_DEGREE
            },
            null,
            null,
            null,
            null,
            TEMPERATURE_TIME + " ASC",
            String.valueOf(limit)
        );
        List<TemperatureReading> temperatures = new ArrayList<TemperatureReading>(limit);
        while (cursor.moveToNext()) {
        	temperatures.add(new TemperatureReading(
                // These numeric indexes relate to the column order
                // defined in the query() call above.
                cursor.getLong(0),
                cursor.getFloat(1)
            ));
        }
 
//        Log.d("getAllPositions()", temperatures.toString());
        db.close();
        return temperatures;
    }
    
    public JSONObject getTemperatureReadingJSON(int limit) throws JSONException {
    	JSONObject temperature = new JSONObject();
        JSONArray temperatureArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_TEMPERATURE,
            new String[] {
        		TEMPERATURE_TIME,
        		TEMPERATURE_DEGREE
            },
            null,
            null,
            null,
            null,
            TEMPERATURE_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("celsius",cursor.getFloat(1));
            temperatureArray.put(reading);
        }
        db.close();
        temperature.put("Temperature",temperatureArray);
        return temperature;
    }
    
    /**
     * Remove all temperature readings for a specific time or earlier from the
     * device-local database.
     *
     * This operation is intended for use after the cloud-based data
     * collection service has confirmed receipt of all such readings.
     *
     * @param time
     *    The UTC time of the newest temperature reading that should be removed
     *    from the device-local database, in milliseconds since 1
     *    January 1970.
     */
    public void deleteTemperatureReadingsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_TEMPERATURE,
        	TEMPERATURE_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }
    
    public JSONObject getTextEventsJSON(int limit) throws JSONException {
    	JSONObject event = new JSONObject();
        JSONArray eventArray = new JSONArray();
    	SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
        		TABLE_TEXTEVENTS,
            new String[] {
        		TEXTEVENTS_TIME,
        		TEXTEVENTS_LABEL
            },
            null,
            null,
            null,
            null,
            TEXTEVENTS_TIME + " ASC",
            String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
        	JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("label",cursor.getString(1));
            eventArray.put(reading);
        }
        db.close();
        event.put("Event",eventArray);
        return event;
    }
    
    public void deleteTextEventsUntil(long time)
    {
    	SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
        	TABLE_TEXTEVENTS,
        	TEXTEVENTS_TIME + " <= ?",
            new String[] { String.valueOf(time) }
        );
        db.close();
    }

    public void addBluetoothReading(String id, long time, short rssi, boolean isLeType){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(BLUETOOTH_ID, id);
        values.put(BLUETOOTH_TIME, time);
        values.put(BLUETOOTH_RSSI, rssi);
        if(isLeType)
            values.put(BLUETOOTH_TYPE, 1);
        else
            values.put(BLUETOOTH_TYPE, 0);

        // 3. insert
        db.insert(TABLE_BLUETOOTH, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public List<BluetoothReading> getBluetoothReading(int limit) {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(
                TABLE_BLUETOOTH,
                new String[] {
                        BLUETOOTH_TIME,
                        BLUETOOTH_ID,
                        BLUETOOTH_RSSI,
                        BLUETOOTH_TYPE
                },
                null,
                null,
                null,
                null,
                BLUETOOTH_TIME + " ASC",
                String.valueOf(limit)
        );
        List<BluetoothReading> bluetooths = new ArrayList<BluetoothReading>(limit);
        while (cursor.moveToNext()) {
            bluetooths.add(new BluetoothReading(
                    // These numeric indexes relate to the column order
                    // defined in the query() call above.
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getShort(2),
                    cursor.getInt(3)
            ));
        }

        db.close();
        return bluetooths;
    }

    public JSONObject getBluetoothJSON(int limit) throws JSONException {
        JSONObject event = new JSONObject();
        JSONArray eventArray = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(
                TABLE_BLUETOOTH,
                new String[] {
                        BLUETOOTH_TIME,
                        BLUETOOTH_ID,
                        BLUETOOTH_RSSI,
                        BLUETOOTH_TYPE
                },
                null,
                null,
                null,
                null,
                BLUETOOTH_TIME + " ASC",
                String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject reading = new JSONObject();
            reading.put("timestamp",cursor.getLong(0));
            reading.put("label",cursor.getString(1));
            reading.put("rssi", cursor.getShort(2));
            if(cursor.getInt(3)==1)
                reading.put("isLEType", "true");
            else
                reading.put("isLEType", "false");
            eventArray.put(reading);
        }
        db.close();
        event.put("Bluetooth",eventArray);
        return event;
    }

    public void deleteBluetoothUntil(long time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
                TABLE_BLUETOOTH,
                BLUETOOTH_TIME + " <= ?",
                new String[] { String.valueOf(time) }
        );
        db.close();
    }

    public void addCellTowerReading(int id, int lac, int mcc, int mnc, long time){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(CELL_ID, id);
        values.put(CELL_MCC, mcc);
        values.put(CELL_MNC, mnc);
        values.put(CELL_LAC, lac);
        values.put(CELL_TIME, time);

        // 3. insert
        db.insert(TABLE_CELLTOWER, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public JSONObject getCellReadingJSON(int limit) throws JSONException {
        JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(
                TABLE_CELLTOWER,
                new String[] {
                        CELL_ID,
                        CELL_MCC,
                        CELL_MNC,
                        CELL_LAC,
                        CELL_TIME
                },
                null,
                null,
                null,
                null,
                CELL_TIME + " ASC",
                String.valueOf(limit)
        );
        while (cursor.moveToNext()) {
            JSONObject readings = new JSONObject();
            readings.put("ScanTime", cursor.getLong(4));
            readings.put("ID", cursor.getInt(0));
            readings.put("LAC", cursor.getInt(1));
            readings.put("MCC", cursor.getInt(2));
            readings.put("MNC", cursor.getInt(3));
            locationArray.put(readings);
        }
        db.close();
        locationScan.put("CellTowerReadings", locationArray);
        return locationScan;
    }

    public void deleteCellReadingsUntil(long time)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(
                TABLE_CELLTOWER,
                CELL_TIME + " <= ?",
                new String[] { String.valueOf(time) }
        );
        db.close();
    }
}
