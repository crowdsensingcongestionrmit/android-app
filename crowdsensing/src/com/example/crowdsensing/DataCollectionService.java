package com.example.crowdsensing;

import android.app.Service;
import android.content.Intent;
import android.hardware.SensorEventListener;
import android.os.IBinder;
import android.util.Log;

import com.example.crowdsensing.sensorcollection.pullsensor.AccelerometerSensor;
import com.example.crowdsensing.sensorcollection.pullsensor.BarometerSensor;
import com.example.crowdsensing.sensorcollection.pullsensor.MagnetometerSensor;
import com.example.crowdsensing.sensorcollection.pullsensor.SensorDataEventListener;
import com.example.crowdsensing.sensorcollection.pullsensor.TemperatureSensor;
import com.example.crowdsensing.sensorcollection.pushsensor.BluetoothSensor;
import com.example.crowdsensing.sensorcollection.pushsensor.CellTowerSensor;
import com.example.crowdsensing.sensorcollection.pushsensor.LocationSensor;
import com.example.crowdsensing.sensorcollection.pushsensor.WifiSensor;

/**
 * This class is used to register sensors and collects sensor data in the Background.
 *
 */
public class DataCollectionService extends Service {

    private WifiSensor wifiSensor;
    private LocationSensor locationSensor;
    private AccelerometerSensor accelerometerSensor;
    private MagnetometerSensor magnetometerSensor;
    private TemperatureSensor temperatureSensor;
    private BarometerSensor barometerSensor;
    private BluetoothSensor bluetoothSensor;
    private CellTowerSensor cellTowerSensor;

    private SensorEventListener sensorListener;

    private MySQLite db;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }


    /**
    * Starts sensor logging when service starts
    */
    @Override
    public void onCreate() {
        super.onCreate();
        db = new MySQLite(this);

        bluetoothSensor = new BluetoothSensor(this,db);
        bluetoothSensor.register();

        wifiSensor = WifiSensor.getInstance(this,db);
        wifiSensor.register();

        locationSensor = new LocationSensor(this, db);
        locationSensor.register();

        sensorListener = new SensorDataEventListener(db);

        accelerometerSensor = new AccelerometerSensor(this, sensorListener);
        accelerometerSensor.register();

        magnetometerSensor = new MagnetometerSensor(this, sensorListener);
        magnetometerSensor.register();

        temperatureSensor = new TemperatureSensor(this, sensorListener);
        temperatureSensor.register();

        barometerSensor = new BarometerSensor(this,sensorListener);
        barometerSensor.register();

        cellTowerSensor = new CellTowerSensor(this,db);
        cellTowerSensor.register();
    }

    /**
    * Stops all sensor update when service is destroyed
    */
    @Override
    public void onDestroy() {
        super.onDestroy();
        wifiSensor.unregister();
        locationSensor.unregister();
        accelerometerSensor.unregister();
        magnetometerSensor.unregister();
        magnetometerSensor.unregister();
        temperatureSensor.unregister();
        barometerSensor.unregister();
        bluetoothSensor.unregister();
        cellTowerSensor.unregister();
        db.close();
    }
}
