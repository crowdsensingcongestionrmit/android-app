package com.example.crowdsensing.sensorreading;

public class TemperatureReading {
    private long time;
    private float degreesCelcius;

    public TemperatureReading(
        long time, float degreesCelcius
    ) {
        this.time = time;
        this.degreesCelcius = degreesCelcius;
    }

    public long getTime() {
        return this.time;
    }

    public float getDegreesCelcius() {
        return this.degreesCelcius;
    }
}
