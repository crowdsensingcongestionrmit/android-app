package com.example.crowdsensing.sensorreading;

/**
 * A snapshot of a location at a specific point in time.
 * <p>
 * {@code GPSReading} instances are created by the
 * {@link com.example.crowdsensing.DataCollectionService} for later
 * submission to the cloud-based web service.
 * </p>
 * <p>
 * Before submission, these are stored in a device-local database by
 * code in {@link com.example.crowdsensing.MySQLite}.
 * </p>
 */
public class GPSReading {
    private double longitude;
    private double latitude;
    private float accuracy;
    private long time;

    /**
     * Construct a new {@code GPSReading} object.
     *
     * @param longitude in degrees.
     * @param latitude in degrees.
     * @param accuracy in metres.
     * @param time in UTC milliseconds since 1 January 1970.
     */
    public GPSReading(
        double longitude, double latitude, float accuracy, long time
    ) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.accuracy = accuracy;
        this.time = time;
    }

    /**
     * A human-readable description of this object.
     *
     * Used solely for debugging purposes.
     */
    public String toString() {
        return "Position [longitude=" + longitude + ", latitude=" + latitude
            + ", accuracy=" + accuracy + ", time=" + time + "]";
    }

    /**
     * The longitude of this reading, in degrees.
     */
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * The latitude of this reading, in degrees.
     */
    public double getLatitude() {
        return this.latitude;
    }

    /**
     * The estimated accuracy of this reading, in metres.
     */
    public float getAccuracy() {
        return this.accuracy;
    }

    /**
     * The UTC time of this reading, in milliseconds since 1 January
     * 1970.
     */
    public long getTime() {
        return this.time;
    }
}
