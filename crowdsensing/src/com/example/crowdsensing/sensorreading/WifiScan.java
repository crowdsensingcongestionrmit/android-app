package com.example.crowdsensing.sensorreading;

import java.util.List;

/**
 * A collection of Wi-Fi access points found over a specific period of
 * time.
 * <p>
 * {@code WifiScan} instances consist of a start and end time
 * representing the period during which the scan was active, and a
 * {@link java.util.Set} of {@link com.example.crowdsensing.sensorreading.WifiReading}
 * instances representing each found Wi-Fi access point.
 * </p>
 * <p>
 * {@code WifiScan} instances are created by the
 * {@link com.example.crowdsensing.DataCollectionService} for later
 * submission to the cloud-based web service.
 * </p>
 * <p>
 * Before submission, these are stored in a device-local database by
 * code in {@link com.example.crowdsensing.MySQLite}.
 * </p>
 */
public class WifiScan {
    private long startTime;
    private long endTime;
    private List<WifiReading> readings;

    /**
     * Construct a new {@code WifiScan} object.
     *
     * @param startTime
     *   The UTC time that the scan commenced, in milliseconds since 1
     *   January 1970.
     * @param endTime
     *   The UTC time that the scan completed, in milliseconds since 1
     *   January 1970.
     * @param readings
     *   A {@link java.util.Set} of
     *   {@link com.example.crowdsensing.sensorreading.WifiReading} instances, each
     *   representing a found Wi-Fi access point.
     */
    public WifiScan(
        long startTime, long endTime, List<WifiReading> readings
    ) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.readings = readings;
    }

    /**
     * A human-readable description of this object.
     *
     * Used solely for debugging purposes.
     */
    public String toString() {
        String information = new String();
        information = "WifiScan [";
        for (WifiReading x : readings) {
            information = information.concat(x.toString());
        }
        return information;
    }

    /**
     * The UTC time that the scan commenced, in milliseconds since 1
     * January 1970.
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * The UTC time that the scan completed, in milliseconds since 1
     * January 1970.
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * A {@link java.util.Set} of
     * {@link com.example.crowdsensing.sensorreading.WifiReading} instances, each
     * representing a found Wi-Fi access point.
     */
    public List<WifiReading> getReadings() {
        return readings;
    }
}
