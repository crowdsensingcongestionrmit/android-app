package com.example.crowdsensing.sensorreading;

public class AccelerometerReading {
    private long time;
    private float xValue;
    private float yValue;
    private float zValue;
    
    public AccelerometerReading(){}

    public AccelerometerReading(long time, float xValue, float yValue, float zValue) 
    {
        this.time = time;
        this.xValue = xValue;
        this.yValue = yValue;
        this.zValue = zValue;
    }

    public long getTime() {
        return this.time;
    }
    
    public float getXValue() {
        return this.xValue;
    }
    
    public float getYValue() {
        return this.yValue;
    }
    
    public float getZValue() {
        return this.zValue;
    }

}
