package com.example.crowdsensing.sensorreading;

public class BluetoothReading {
    private long time;
    private String id;
    private short rssi;
    private boolean isLEType;

    public BluetoothReading(
            long time, String id, short rssi, int isLEType
    ) {
        this.time = time;
        this.id = id;
        this.rssi = rssi;
        if(isLEType==1)
            this.isLEType = true;
        else
            this.isLEType = false;
    }

    /**
     * A human-readable description of this object.
     *
     * Used solely for debugging purposes.
     */
    public String toString() {
        return "Bluetooth [time=" + time + ", id=" + id + ", rssi=" + rssi + "]";
    }

    /**
     * The UTC time of this reading, in milliseconds since 1 January
     * 1970.
     */
    public long getTime() {
        return this.time;
    }

    public String getID(){
        return this.id;
    }

    public short getRSSI(){
        return this.rssi;
    }

    public boolean getIsLEType(){
        return this.isLEType;
    }
}
