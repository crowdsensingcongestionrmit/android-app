package com.example.crowdsensing.sensorreading;

public class MagnetometerReading {
    private long time;
    private float xValue;
    private float yValue;
    private float zValue;
    
    public MagnetometerReading(){}

    public MagnetometerReading(long time, float xValue, float yValue, float zValue) 
    {
        this.time = time;
        this.xValue = xValue;
        this.yValue = yValue;
        this.zValue = zValue;
    }

    public long getTime() {
        return this.time;
    }
    
    public void setTime(long time)
    {
    	this.time = time;
    }
    
    public float getXValue() {
        return this.xValue;
    }
    
    public void setXValue(float value)
    {
    	this.xValue =  value;
    }
    
    public float getYValue() {
        return this.yValue;
    }
    
    public void setYValue(float value)
    {
    	this.yValue =  value;
    }
    
    public float getZValue() {
        return this.zValue;
    }
    
    public void setZValue(float value)
    {
    	this.zValue =  value;
    }

}
