package com.example.crowdsensing.sensorreading;

/**
 * A representation of one Wi-Fi access point found during a
 * {@link WifiScan}.
 * <p>
 * {@code WifiReading} instances consist of a MAC address, network
 * name, signal level and detection time, and form part of a collection
 * of readings encapsulated in a {@link WifiScan}
 * instance.
 * </p>
 * <p>
 * {@code WifiReading} instances are created by the
 * {@link com.example.crowdsensing.DataCollectionService} for later
 * submission to the cloud-based web service.
 * </p>
 * <p>
 * Before submission, these are stored in a device-local database by
 * code in {@link com.example.crowdsensing.MySQLite}.
 * </p>
 */
public class WifiReading {
    private String macAddress;
    private String name;
    private int signalLevel;
    private long time;

    /**
     * Construct a new {@code WifiReading} object.
     *
     * @param macAddress
     *   The MAC address of this Wi-Fi access point.
     * @param name
     *   The network name of this Wi-Fi access point.
     * @param signalLevel
     *   The detected signal level of this Wi-Fi access point in dBm.
     * @param time
     *   The UTC time at which this Wi-Fi access point was detected
     *   during the {@link WifiScan}, in
     *   milliseconds since 1 January 1970.
     */
    public WifiReading(
        String macAddress, String name, int signalLevel, long time
    ) {
        this.macAddress = macAddress;
        this.name = name;
        this.signalLevel = signalLevel;
        this.time = time;
    }

    /**
     * A human-readable description of this object.
     *
     * Used solely for debugging purposes.
     */
    public String toString() {
        return "Wifi Reading MAC: " + macAddress;
    }

    /**
     * The MAC address of this Wi-Fi access point.
     */
    public String getMacAddress() {
        return macAddress;
    }

    /**
     * The network name of this Wi-Fi access point.
     */
    public String getName() {
        return name;
    }

    /**
     * The detected signal level of this Wi-Fi access point in dBm.
     */
    public int getSignalLevel() {
        return signalLevel;
    }

    /**
     * The UTC time at which this Wi-Fi access point was detected
     * during the {@link WifiScan}, in
     * milliseconds since 1 January 1970.
     */
    public long getTime() {
        return time;
    }
}
