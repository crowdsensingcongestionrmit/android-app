package com.example.crowdsensing.sensorreading;

public class BarometerReading {
    private long time;
    private float pressure;

    public BarometerReading(long time, float pressure)
    {
        this.time = time;
        this.pressure = pressure;
    }

    public long getTime() {
        return this.time;
    }

    public float getPressure() {
        return this.pressure;
    }
}
