package com.example.crowdsensing.json;

import com.example.crowdsensing.sensorreading.AccelerometerReading;
import com.example.crowdsensing.sensorreading.BarometerReading;
import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.MagnetometerReading;
import com.example.crowdsensing.sensorreading.TemperatureReading;
import com.example.crowdsensing.sensorreading.WifiReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/*
* This class implements the method to build JSON for different sensor data to be sent to web server
*
* */
public class JSONBuilder implements JSONBuilderInterface{

    /*
    * Builds JSON for wifi data in the format to be sent to web server
    *
    * @param wifiScanCollection A collection of wifiScan
    *
    * @return JSON  JSONObject of the Collection of wifiScan in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildWiFiJSON(Collection<WifiScan> wifiScanCollection) throws JSONException{
        JSONArray wifiScanArray = new JSONArray();

        for(WifiScan wifiScan : wifiScanCollection){
            JSONObject wifiScanJSON = new JSONObject();
            JSONArray wifiScanReadingArray = new JSONArray();
            wifiScanJSON.put("ScanStartTime", wifiScan.getStartTime());
            wifiScanJSON.put("ScanEndTime", wifiScan.getEndTime());


            for(WifiReading r : wifiScan.getReadings()){
                JSONObject readings = new JSONObject();
                readings.put("ScanTime", r.getTime());
                readings.put("MacAddress", r.getMacAddress());
                readings.put("NetworkName", r.getName());
                readings.put("Signal", r.getSignalLevel());
                wifiScanReadingArray.put(readings);
            }
            wifiScanJSON.put("WifiReading", wifiScanReadingArray);
            wifiScanArray.put(wifiScanJSON);
        }

        // Callers require the scans to be wrapped in an object with a
        // specific key.
        return new JSONObject().put("WifiScans", wifiScanArray);
    }

    /*
    * Builds JSON for gps data in the format to be sent to web server
    *
    * @param gpsCollection A collection of GPSReading to be convert to JSON
    *
    * @return JSON  JSONObject of the Collection of GPSReading in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildLocationJSON(Collection<GPSReading> gpsCollection) throws JSONException {
        JSONObject locationScan = new JSONObject();
        JSONArray locationArray = new JSONArray();

        for(GPSReading r : gpsCollection){
            JSONObject readings = new JSONObject();
            readings.put("ScanTime", r.getTime());
            readings.put("Longitude", r.getLongitude());
            readings.put("Latitude", r.getLatitude());
            readings.put("Accuracy", r.getAccuracy());
            locationArray.put(readings);
        }

        locationScan.put("GPSReadings", locationArray);

        return locationScan;
    }

    /*
    * Builds JSON for gps data in the format to be sent to web server
    *
    * @param gpsCollection A collection of GPSReading to be convert to JSON
    *
    * @return JSON  JSONObject of the Collection of GPSReading in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildAccelerometerJSON(Collection<AccelerometerReading> accelerometerCollection) throws JSONException{
        JSONObject accelerometer = new JSONObject();
        JSONArray accelerometerArray = new JSONArray();

        for(AccelerometerReading r : accelerometerCollection){
            JSONObject reading = new JSONObject();
            reading.put("timestamp",r.getTime());
            reading.put("x",r.getXValue());
            reading.put("y",r.getYValue());
            reading.put("z",r.getZValue());
            accelerometerArray.put(reading);
        }

        accelerometer.put("Accelerometer",accelerometerArray);

        return accelerometer;
    }

    /*
    * Builds JSON for gps data in the format to be sent to web server
    *
    * @param gpsCollection A collection of GPSReading to be convert to JSON
    *
    * @return JSON  JSONObject of the Collection of GPSReading in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildMagnetometerJSON(Collection<MagnetometerReading> magnetometerCollection) throws JSONException {
        JSONObject magnetometer = new JSONObject();
        JSONArray magnetometerArray = new JSONArray();

        for(MagnetometerReading r : magnetometerCollection){
            JSONObject reading = new JSONObject();
            reading.put("timestamp",r.getTime());
            reading.put("x",r.getXValue());
            reading.put("y",r.getYValue());
            reading.put("z",r.getZValue());
            magnetometerArray.put(reading);
        }

        magnetometer.put("Magnetometer",magnetometerArray);

        return magnetometer;
    }

    /*
    * Builds JSON for gps data in the format to be sent to web server
    *
    * @param gpsCollection A collection of GPSReading to be convert to JSON
    *
    * @return JSON  JSONObject of the Collection of GPSReading in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildBarometerJSON(Collection<BarometerReading> barometerCollection) throws JSONException {
        JSONObject barometer = new JSONObject();
        JSONArray barometerArray = new JSONArray();

        for(BarometerReading r : barometerCollection){
            JSONObject reading = new JSONObject();
            reading.put("timestamp",r.getTime());
            reading.put("pressure",r.getPressure());
            barometerArray.put(reading);
        }

        barometer.put("Barometer",barometerArray);

        return barometer;
    }

    /*
    * Builds JSON for gps data in the format to be sent to web server
    *
    * @param gpsCollection A collection of GPSReading to be convert to JSON
    *
    * @return JSON  JSONObject of the Collection of GPSReading in the for to be sent to web server
    *
    * */
    @Override
    public JSONObject buildTemperatureJSON(Collection<TemperatureReading> temperatureCollection) throws JSONException {
        JSONObject temperature = new JSONObject();
        JSONArray temperatureArray = new JSONArray();

        for(TemperatureReading r : temperatureCollection){
            JSONObject reading = new JSONObject();
            reading.put("timestamp",r.getTime());
            reading.put("celsius",r.getDegreesCelcius());
            temperatureArray.put(reading);
        }

        temperature.put("Temperature",temperatureArray);

        return temperature;
    }


}
