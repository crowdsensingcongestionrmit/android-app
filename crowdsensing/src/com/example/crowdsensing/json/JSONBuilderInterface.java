package com.example.crowdsensing.json;

import com.example.crowdsensing.sensorreading.AccelerometerReading;
import com.example.crowdsensing.sensorreading.BarometerReading;
import com.example.crowdsensing.sensorreading.GPSReading;
import com.example.crowdsensing.sensorreading.MagnetometerReading;
import com.example.crowdsensing.sensorreading.TemperatureReading;
import com.example.crowdsensing.sensorreading.WifiScan;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/*
* This interface defines methods for building JSON object for different sensor data to be sent to
* web server
*
* */
public interface JSONBuilderInterface {

    public JSONObject buildWiFiJSON(Collection<WifiScan> wifiScanCollection)throws JSONException;
    public JSONObject buildLocationJSON(Collection<GPSReading> gpsCollection) throws JSONException;
    public JSONObject buildAccelerometerJSON(Collection<AccelerometerReading> accelerometerCollection)throws JSONException;
    public JSONObject buildMagnetometerJSON(Collection<MagnetometerReading> magnetometerCollection)throws JSONException;
    public JSONObject buildBarometerJSON(Collection<BarometerReading> barometerCollection)throws JSONException;
    public JSONObject buildTemperatureJSON(Collection<TemperatureReading> temperatureCollection)throws JSONException;

}
